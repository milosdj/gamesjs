
function FourInLine (outerElem) {

	if (!outerElem || typeof outerElem !== 'object') {
		console.log('No outer elem: '+ outerElem);	
		return false;
	}	
	


let dropDown = false;
let boardCols = 7;
let boardRows = 6;
let totalSquares = boardCols * boardRows;
let squareCounter = 0;
const squares = [];	//new Array(boardCols).fill([]);

const container = document.createElement('DIV');
const menu = new Menu(container);
const boardElem = document.createElement('DIV');

function V (x,y) {
	this.x = x;
	this.y = y;
}
const dir = [new V(-1, 1), new V(0,1), new V(1,1),	// up l, up, up r
new V(-1, 0), new V(1,0),	// left, right
new V(-1, -1), new V(0,-1), new V(1,-1),
];

const players = [0,
{'color': '#FF0000', id:1, name:'Red'},	// 1
{'color': '#0000FF', id:2, name:'Blue'}	// 2
];
let currentPlayer = Math.round(Math.random() + 1);
let inGame = true;




function init () {

	container.style.boxSizing = 'border-box';
	container.style.userSelect = 'none';
	container.style.overflow = 'hidden';
	container.style.position = 'relative';
	container.style.minWidth = '320px';
	container.style.minHeight = '320px';
	container.style.width = '100%';
	container.style.height = '100%';
	container.style.display = 'flex';
	container.style.flexFlow = 'column nowrap';
	container.style.borderRadius = '0.5rem';
	container.style.backgroundColor = '#eeeeee';
//	container.style.alignItems = 'center';
	container.style.justifyContent = 'center';

	menu.setPlayer(currentPlayer);

	boardElem.style.boxSizing = 'border-box';
	boardElem.style.userSelect = 'none';
	boardElem.style.overflow = 'hidden';
	boardElem.style.position = 'relative';
//	boardElem.style.alignSelf = 'center';
	boardElem.style.margin = 'auto';
	boardElem.style.width = '100%';
	boardElem.style.display = 'flex';
	boardElem.style.flexFlow = 'column nowrap';
	boardElem.style.flexGrow = 1;
	boardElem.style.maxWidth = '100vmin';
	boardElem.style.maxHeight = '100vmin';
	boardElem.style.justifyContent = 'flex-start';
	boardElem.style.alignContent = 'center';
	boardElem.style.backgroundColor = '#ddd';

	boardElem.addEventListener('click', boardClicked);

	createTableGrid();
	container.appendChild(boardElem);

	outerElem.innerHTML = '';
	outerElem.appendChild(container);
}// ----------------------------- end init () ----------------------------------


function createTableGrid () {

	let frag = document.createDocumentFragment(),
	row, div;

	squares.length = 0;
	boardElem.innerHTML = '';

	for (let r = 0; r < boardRows; ++r)
	{
		squares[r] = [];
		row = document.createElement('DIV');
//		row.style.border = '1px solid blue';
		row.style.display = 'flex';
		row.style.flexFlow = 'row nowrap';
		row.style.flex = 1;
		row.style.overflow = 'hidden';
		row.style.position = 'relative';

		for (let c = 0; c < boardCols; ++c)
		{
			let sq = new Square (r * boardCols + c);		// id++
			squares[r][c] = sq;
			div = document.createElement('DIV');
			div.style.display = 'flex';
			div.style.justifyContent = 'center';
			div.style.alignItems = 'center';

			div.style.flex = 1;
			div.style.overflow = 'hidden';
			div.style.position = 'relative';
			div.style.border = '1px solid gray';
			div.appendChild(sq.getElem());
			row.appendChild(div);
		}
		frag.appendChild(row);
	}

	boardElem.appendChild(frag);

	frag = row = div = null;
}

function boardClicked (e) {

	if (!inGame)	return;
	let id = parseInt(e.target.dataset.id, 10);
	if (! (0 <= id && id <= totalSquares) )	return;

	let r = Math.floor(id/boardCols);
	let c = id % boardCols;

	if (dropDown)
	{
		r = findFreeRow(c);
		if (r < 0 )	return;
	}

	if (squares[r][c].Taken())	return;		// already taken square


	squares[r][c].setColor(currentPlayer);

	++squareCounter;
	if (squareCounter >= totalSquares)	gameOver();


	let winScore = checkWin(r, c);
	if (winScore)
	{
		inGame = false;
		playerWon(currentPlayer, winScore);
	}

	// change player
	currentPlayer %= 2;
	++currentPlayer;
	menu.setPlayer(currentPlayer);
}


function checkWin (r, c) {

	let score = 0,
	sum8 = [],		// u 8 smerova
	sum4 = [];		// u 4 pravca

	for (let i = 0; i < 8; ++i)
	{
		sum8[i] = 0;
		let move = dir[i];
		let nextRow = r - move.y;
		let nextCol = c + move.x;

		while ((0 <= nextCol && nextCol < boardCols) &&
			(0 <= nextRow && nextRow < boardRows) &&
			squares[nextRow][nextCol].isPlayer(currentPlayer))
		{
			++sum8[i];
			nextCol += move.x;
			nextRow -= move.y;
		}
	}
	sum4[0] = sum8[1] + sum8[6] + 1;		// u 1 d 6 
	sum4[1] = sum8[3] + sum8[4] + 1;		// l 3 r 4
	sum4[2] = sum8[0] + sum8[7] + 1;		// ul 0 dr 7
	sum4[3] = sum8[2] + sum8[5] + 1;		// ur 2 dl 5

	for (let s of sum4)
	{
		if (s >= 4)	score += s;
	}
	return score;
}

function findFreeRow (c) {

	let rowId = boardRows - 1;

	while (rowId >= 0) {
	
		if (!squares[rowId][c].Taken())		return rowId;
		--rowId;
	}

	return -1;
}

function newGame () {

	// dont change player! player is auto changed in tableClicked
	menu.setPlayer(currentPlayer);
	inGame = true;
	squareCounter = 0;

	for (let r = 0; r < boardRows; ++r)
		for (let c = 0; c < boardCols; ++c)
			squares[r][c].reset();
}

function gameOver () {

gl.msgBox(`
Draw!

Ok for New game`, 'okc', function (ok){

	if (ok) newGame();
});


}

function playerWon (cp, score) {

	gl.msgBox(`${players[cp].name} player win!!!

Score: ${score}`, 'ok', newGame);

};


function Square (id) {

	let taken = 0;;
	const elem = document.createElement('DIV');

function init () {

	elem.style.display = 'inline-block';
	elem.style.boxSizing = 'border-box';
	elem.style.width =  '90%';
	elem.style.height = '90%';
	elem.style.border = '1px solid #cfcfcf';
	elem.style.borderRadius = '50%';
	elem.dataset.id = id;
}

this.setColor = function (cp) {

	if (taken < 0)	return;

	taken = players[cp].id;
	elem.style.backgroundColor = players[cp].color;;
};

this.reset = function () {
	taken = 0;
	elem.style.backgroundColor = '';

};
this.getElem = function () { return elem;};
this.Taken = function () {return taken};
this.isPlayer = function (cp) {return cp === taken};

init();
}


function Menu (container) {

	const menuElem = document.createElement('DIV');

	const fullScr = new gl.FullScreenButton(container);

	const playerElem = document.createElement('SPAN');

	const dropDownElem = document.createElement('input');
	
	const boardSizeElem = document.createElement('input');
	const displaySize = document.createElement('DIV');


function init () {

	var wrap, div, label;

	menuElem.style.userSelect = 'none';
	menuElem.style.overflow = 'hidden';
	menuElem.style.position = 'relative';
	menuElem.style.display = 'flex';
	menuElem.style.justifyContent = 'space-around';
	menuElem.style.padding = '0.5rem';
	menuElem.style.height = '3rem';
	menuElem.style.backgroundColor = '#ccc';


	wrap = document.createElement('DIV');
	wrap.style.display = 'flex';
	wrap.style.flex = 1;
	wrap.style.justifyContent = 'space-between';

	// FULL SCREEN BUTTON
	wrap.appendChild(fullScr.getElem());


	// drop down input
	label = document.createElement('LABEL');
	label.style.marginLeft = '0.5rem';
	label.style.borderRadius = '0.5rem';
	label.style.border = '1px solid darkgray';
	label.style.display = 'flex';
//	label.style.flex = 1;
	label.style.padding = '0.5rem';
	label.style.justifyContent = 'center';
	label.style.alignItems = 'center';
	label.innerHTML = 'Drop Down';
	dropDownElem.type = 'checkBox';
	dropDownElem.checked = dropDown;
	dropDownElem.addEventListener('input', function (e) {

		dropDown = !!e.target.checked;
	});
	label.append(dropDownElem);
	wrap.appendChild(label);
	menuElem.appendChild(wrap);


	// players
	wrap = document.createElement('DIV');
	wrap.style.display = 'flex';
	wrap.style.flex = 1;
	wrap.style.justifyContent = 'center';
	wrap.style.alignItems = 'center';

	div = document.createElement('DIV');
	div.innerHTML = 'Player: ';
	playerElem.style.fontSize = '200%';
	div.appendChild(playerElem);
	wrap.appendChild(div);
	menuElem.appendChild(wrap);


	// size
	wrap = document.createElement('DIV');
	wrap.style.display = 'flex';
	wrap.style.flex = 1;
	wrap.style.flexFlow = 'column nowrap';
	wrap.style.justifyContent = 'center';
	wrap.style.alignItems = 'center';

	displaySize.innerHTML = boardRows + 'x' + boardCols + ' = ' + totalSquares;
	wrap.appendChild(displaySize);

	boardSizeElem.type = 'range';
	boardSizeElem.style.width = '100%';
	boardSizeElem.max = 25;
	boardSizeElem.min = 5;
	boardSizeElem.value = 6;
	boardSizeElem.addEventListener('input', function (e) {
	
		boardRows = parseInt(e.target.value, 10);
		boardCols = boardRows + 1;
		totalSquares = boardRows * boardCols;
		createTableGrid();

		displaySize.innerHTML = boardRows + 'x' + boardCols + ' = ' + totalSquares;
		// change board size
	})

	wrap.appendChild(boardSizeElem);

	menuElem.appendChild(wrap);
	container.appendChild(menuElem);

	wrap = div = label = null;
}

this.setPlayer = function (cp) {
	playerElem.innerHTML = players[cp].name;
	playerElem.style.color = players[cp].color;
};

init();
}// ------------------------------- end Menu()----------------------------------


init();
} // ------------------ end FourInLine ()-------------------------
