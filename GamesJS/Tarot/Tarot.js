
function Tarot (outerElem) {

	if (!outerElem || typeof outerElem !== 'object') {
		console.log('No outer elem: '+ outerElem);	
		return false;
	}	
	
let soundVolume = 1;
const container = document.createElement('DIV');
const menuElem = new Menu(container);
const boardElem = document.createElement('DIV');
const cards = [];

const data = [
{img:'1.png', mp3:'1.mp3'},
{img:'2.png', mp3:'2.mp3'},
{img:'3.png', mp3:'3.mp3'},
{img:'4.png', mp3:'4.mp3'},
{img:'5.png', mp3:'5.mp3'},
{img:'6.png', mp3:'6.mp3'},
{img:'7.png', mp3:'7.mp3'},
{img:'8.png', mp3:'8.mp3'},
{img:'9.png', mp3:'9.mp3'},
];


function init () {

	container.style.boxSizing = 'border-box';
	container.style.userSelect = 'none';
	container.style.overflow = 'hidden';
	container.style.position = 'relative';
//	container.style.minWidth = '320px';
//	container.style.minHeight = '320px';
	container.style.width = '100%';
	container.style.height = '100%';
	container.style.display = 'flex';
	container.style.flexFlow = 'column nowrap';
	container.style.borderRadius = '1%';



	boardElem.style.boxSizing = 'border-box';
	boardElem.style.userSelect = 'none';
	boardElem.style.overflow = 'hidden';
	boardElem.style.position = 'relative';
	boardElem.style.width = '100%';
	boardElem.style.height = '100%';
	boardElem.style.display = 'flex';
	boardElem.style.flexFlow = 'row wrap';
	boardElem.style.justifyContent = 'space-around';
	boardElem.style.alignContent = 'space-around';
	boardElem.style.flexGrow = 1;
	boardElem.style.backgroundColor = '#cacadd';
	boardElem.addEventListener('click', boardElemOnClick);
	container.appendChild(boardElem);

	outerElem.innerHTML = '';
	outerElem.appendChild(container);


	for (let i = 0; i < data.length; ++i)
	{
		let d = data[i];
		let card = new Card(i, d.img, d.mp3);
		cards[i] = card;
		boardElem.appendChild(card.getElem());
		card = null;
	}

}// ---------------------- end init () -----------------------


function boardElemOnClick (e) {

	let elem = e.target;
	while (elem.nodeName == 'IMG' && elem.nodeName != 'HTML')	elem = elem.parentNode;

	let id = parseInt(elem.dataset.id, 10);

//console.log(id);
	if (!(0 <= id && id <= 11)) return;


	cards[id].big();

	elem = null;
}



function Card (id, img, mp3) {

	const cardElem = document.createElement('DIV');
	const bigCardElem = document.createElement('DIV');
	const audioElem = new Audio();
	const imgElem = document.createElement('IMG');


function init () {

	audioElem.src = 'Tarot/snd/' + mp3;
	audioElem.preload = 'auto';
	audioElem.volume = soundVolume;
	audioElem.addEventListener('ended', function () {	
		closeBigCard();
	});

	cardElem.style.position = 'relative';
	cardElem.boxSizing = 'border-box';
	cardElem.style.border = '1px solid darkgray';
	cardElem.style.borderRadius = '2%';
	cardElem.dataset.id = id;
	cardElem.style.display = 'flex';
//	cardElem.style.flex = 1;
	cardElem.style.backgroundImage = "url(Tarot/img/cloud.jpg)";
	cardElem.style.backgroundRepeat = 'no-repeat';
	cardElem.style.backgroundPosition = 'center';
	cardElem.style.backgroundSize = 'cover';
	cardElem.style.height = '32%';
	cardElem.style.width = '32%';

	bigCardElem.style.userSelect = 'none';
	bigCardElem.style.display = 'flex';
	bigCardElem.style.position = 'absolute';
	bigCardElem.style.width = '100%';
	bigCardElem.style.height = '100%';
	bigCardElem.style.zIndex = 1000;
	bigCardElem.style.backgroundColor = '#cacadd';
	bigCardElem.addEventListener('click', closeBigCard);


	imgElem.src = 'Tarot/img/'+ img;
	imgElem.style.height = '100%';
	imgElem.style.width = 'auto';
	imgElem.style.margin = 'auto';

	bigCardElem.appendChild(imgElem);
}

this.big = function () {

	boardElem.appendChild(bigCardElem);
	audioElem.currentTime = 0;
	audioElem.play();
};

function closeBigCard () {

	bigCardElem.parentNode.removeChild(bigCardElem);

	audioElem.pause();
	audioElem.currentTime = 0;
}



this.getElem = function () {
	return cardElem;
};
this.setVolume = function (v) {
	audioElem.volume = v;
};

init();
}// ------------------------------ end Card () ---------------------------------



function Menu (container) {

	const menuElem = document.createElement('DIV');

	const fullScreenButton = document.createElement('BUTTON');

	const volumeElem = document.createElement('input');

function init () {

	
	window.addEventListener('resize',  function () {

		if (document.fullscreenElement)
		{
			fullScreenButton.innerHTML = 'EXIT';

		} else
		{
			fullScreenButton.innerHTML = 'FULL';
		}
});



	var wrap;

	menuElem.style.userSelect = 'none';
	menuElem.style.overflow = 'hidden';
	menuElem.style.position = 'relative';
	menuElem.style.display = 'flex';
	menuElem.style.justifyContent = 'space-around';
	menuElem.style.padding = '0.5em';
	menuElem.style.backgroundColor = '#00000010';
//	menuElem.innerHTML = 'menu';

	wrap = document.createElement('DIV');
	wrap.style.display = 'flex';
	wrap.style.flex = 1;
	wrap.style.justifyContent = 'space-around';
//	wrap.style.border = '1px solid red'
//	wrap.style.height = '100%';



	// FULL SCREEN
	fullScreenButton.style.display = 'flex';
	fullScreenButton.style.flex = 1;
	fullScreenButton.style.justifyContent = 'center';
	fullScreenButton.style.alignItems = 'center';
	fullScreenButton.style.height = '2rem';
	fullScreenButton.innerHTML = 'FULL';
	fullScreenButton.addEventListener('click', function () {

		if (document.fullscreenElement)
		{
			fullScreenButton.innerHTML = 'FULL';
			document.exitFullscreen();
		}
		else
		{
			fullScreenButton.innerHTML = 'EXIT';
			var promise = boardElem.requestFullscreen();
/*
			promise.then(function () {
			canvas.width = canvas.clientWidth;
			canvas.height = canvas.clientHeight;
			});
*/
		}
	});
	wrap.appendChild(fullScreenButton);
	menuElem.append(wrap);

	wrap = document.createElement('DIV');
	wrap.style.display = 'flex';
	wrap.style.flex = 3;
	wrap.style.justifyContent = 'flex-end';
	wrap.style.alignItems = 'center';

	volumeElem.type = 'range';
	volumeElem.style.marginLeft = '3rem';
	volumeElem.style.width = '100%';
	volumeElem.max = 100;
	volumeElem.min = 0;
	volumeElem.value = 100;
	volumeElem.addEventListener('input', function (e) {
	
		let vol = parseInt(e.target.value, 10) / 100;

		for (let card of cards)
		{
			card.setVolume(vol);
		}
	});

	wrap.appendChild(volumeElem);

	menuElem.appendChild(wrap);
	container.appendChild(menuElem);

	wrap = null;
}
init();
}// ------------------------------- end Menu()----------------------------------


init();
} // --------------------------- end Tarot () ----------------------------------
