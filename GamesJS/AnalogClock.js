/*
14.4.2020



*/
function AnalogClock (outerElem, twentyFour, showSecondsArm) {
'use strict';
	if (!outerElem || typeof outerElem !== 'object') {
		return false;
	}

	twentyFour = parseInt(twentyFour, 10) || 12;
//	twentyFour = twentyFour == 24 ? twentyFour : 12;
	
	showSecondsArm = !!showSecondsArm;	// show seconds
	
	const clockElem = document.createElement('DIV'),
	hoursArm = document.createElement('DIV'),
	minutesArm = document.createElement('DIV'),
	secondsArm = document.createElement('DIV')
	;
	
	
	let ss = {}, setIntervalId,
	intervalTime = showSecondsArm ? 500 : 30 * 1000,
	tmp
	;

function Menu () {
	



}// ------------ end Menu ------------
Menu.setStyle = function (target, source) {
	
	for (let item in source) {
		
		if (source.hasOwnProperty(item) && target.hasOwnProperty(item)) {
			
			target[item] = Menu.getValue(source[item]);
			
		} else {
			
			//console.error(item);
		}
	}
};
Menu.getValue = function (obj) {
	// source[item] instanceof 
	if (typeof obj === 'object' && obj !== null && obj.hasOwnProperty('value'))		return obj.value;
	return obj;
};
Menu.SI = function SI (v='', it='', d='', t='') {
	this.value = v;	// full css value #000000 or rgb(0,0,0)
	this.inputType = it;
	this.description = d;
	this.txt = t;	// for user...??? never used???
};
//console.log(Menu.SI);


ss.general = {
style : {
	width : new Menu.SI(600, 'number', 'Clock Width'),
	height : new Menu.SI(600, 'number', 'Clock Height'),
	backgroundColor : new Menu.SI('#d3d3d3', 'color', 'Clock background color'),
	border : '1px solid white',
	overflow : 'hidden',
}
};

ss.hoursArm = {
style : {
	zIndex: 2,
	width : 10,
	height : Menu.getValue(ss.general.style.height) / 2 * 0.75,
	backgroundColor : new Menu.SI('#0088FF', 'color', 'Arm color'),
	borderRadius : new Menu.SI('10px', 'text', 'Border radius'),
}
};
	
ss.minutesArm = {
style : {
	zIndex: 2,
	width : 8,
	height : Menu.getValue(ss.general.style.height) / 2 * 0.9,
	backgroundColor : new Menu.SI('#00FFFF', 'color', 'Arm color'),
	borderRadius : '8px',
}
};

ss.secondsArm = {
style : {
	zIndex : 2,
	width : 4,
	height : Menu.getValue(ss.general.style.height) / 2 + 10,
	backgroundColor : new Menu.SI('#FF2222', 'color', 'Arm color'),
	borderRadius : '4px',
}
};	

ss.hoursNumbers = {
style : {
	zIndex : 10,
	numberRadiusPercent : 1.3,
	rPercent : 0.7,
	fontSize : 30,
	textAlign : 'center',
	verticalAlign : 'middle',
	backgroundColor : 'transparent',
	border : '1px solid darkgray',
	borderRadius : '50%',
}
};

tmp = Menu.getValue(ss.hoursNumbers.style.fontSize) * Menu.getValue(ss.hoursNumbers.style.numberRadiusPercent);
ss.hoursNumbers.style.width = tmp;
ss.hoursNumbers.style.height = tmp;
ss.hoursNumbers.style.lineHeight = tmp + 'px';



ss.minutesNumbers = {
style : {
	zIndex : 10,
	numberRadiusPercent : 1.4,
	rPercent : 0.92,	// 0-1
	fontSize : 30,
	backgroundColor : 'transparent',
	border : '1px solid darkgray',
	borderRadius : '50%',
	
}
};
tmp = Menu.getValue(ss.minutesNumbers.style.fontSize) * Menu.getValue(ss.minutesNumbers.style.numberRadiusPercent);
ss.minutesNumbers.style.width = tmp;
ss.minutesNumbers.style.height = tmp;
ss.minutesNumbers.style.lineHeight = tmp + 'px';


ss.Dot = {
style : {
	numberRadiusPercent : 1.3,
	rPercent : 1,	// 0-1
	fontSize : 6,
	zIndex : 1,
	backgroundColor : 'darkgray',
	border : '1px solid darkgray',
	borderRadius : '50%',
}
};
tmp = Menu.getValue(ss.Dot.style.fontSize);
ss.Dot.style.width = tmp;
ss.Dot.style.height = tmp;
ss.Dot.style.lineHeight = tmp + 'px';


function init () {
	
	let clockWidth = Menu.getValue(ss.general.style.width),
	clockHeight = Menu.getValue(ss.general.style.height),
	clockNumbers
	;
	
	clockElem.style.position = 'relative';
	Menu.setStyle(clockElem.style, ss.general.style);
	
	clockNumbers = createNumbers(twentyFour, Menu.getValue(ss.hoursNumbers.style.rPercent), 1);	// 24h
	clockElem.appendChild(clockNumbers);

	clockNumbers = createNumbers(60, Menu.getValue(ss.minutesNumbers.style.rPercent), 5, 'dot');	// 60min
	clockElem.appendChild(clockNumbers);
	
	
	hoursArm.style.position = 'absolute';
	hoursArm.style.bottom = clockHeight / 2;
	hoursArm.style.left = clockWidth / 2 - Menu.getValue(ss.hoursArm.style.width) / 2;
	hoursArm.style.transformOrigin = "center bottom";
	Menu.setStyle(hoursArm.style, ss.hoursArm.style);
	clockElem.appendChild(hoursArm);

	
	minutesArm.style.position = 'absolute';
	minutesArm.style.bottom = clockHeight / 2;
	minutesArm.style.left = clockWidth / 2 - Menu.getValue(ss.minutesArm.style.width) / 2;
	minutesArm.style.transformOrigin = "center bottom";
	Menu.setStyle(minutesArm.style, ss.minutesArm.style);
	clockElem.appendChild(minutesArm);
	

	secondsArm.style.position = 'absolute';
	secondsArm.style.bottom = clockHeight / 2;
	secondsArm.style.left = clockWidth / 2 - Menu.getValue(ss.secondsArm.style.width) / 2;
	secondsArm.style.transformOrigin = "center bottom";
	Menu.setStyle(secondsArm.style, ss.secondsArm.style);
	clockElem.appendChild(secondsArm);

		
	animate();
	setIntervalId = setInterval(animate, intervalTime);

	outerElem.innerHTML = '';
	outerElem.appendChild(clockElem);
}

this.getElem = function () {
	return clockElem;
};

/*
numbers - how much numbers 12, 24

p - arm length in %  0-1

skip	 1 = 1,2,3... 5 = 5, 10, 15,...

mark 'dot', 'line'


className
fonttype fontSize, color, italic, bold, 
borderradius, color, bcngcolor
digit size

*/
function createNumbers (numbers, p, skip, mark) {

	skip = parseInt(skip, 10) || 1;

	p = parseFloat(p, 10) || 0.8;
	p = 0 <= p && p <= 1.5 ? p : p/100;

	
	let f = document.createDocumentFragment(),
	clockWidth = Menu.getValue(ss.general.style.width),
	clockHeight = Menu.getValue(ss.general.style.height),
	
	r = clockWidth /2 * p
	;
//debugger;	
	for (let i = 0; i < numbers; ++i) {
		
		let ang = i / (numbers/2) * Math.PI ;	//  n/2 za 12 i 24
		ang -= Math.PI/2;	// rotacija ulevo za 90 stepeni
		let fontSize = Menu.getValue(ss.minutesNumbers.style.fontSize);
		let numberRadiusPercent = Menu.getValue(ss.minutesNumbers.style.numberRadiusPercent);

		if (i % skip !== 0) {
			//console.log(skip);
			let d = document.createElement('DIV');
			let dotFontSize = Menu.getValue(ss.Dot.style.fontSize);
			d.style.position = 'absolute';
			d.style.top = Math.sin(ang) * r + clockWidth / 2 - dotFontSize  / 2;
			d.style.left = Math.cos(ang) * r + clockHeight / 2 - dotFontSize /2;
			Menu.setStyle(d.style, ss.Dot.style);
			
			f.appendChild(d);
			continue;
		}
		
		fontSize = Menu.getValue(ss.hoursNumbers.style.fontSize);
		numberRadiusPercent = Menu.getValue(ss.hoursNumbers.style.numberRadiusPercent);
		
		let e = document.createElement('DIV');
		e.style.position = 'absolute';
		e.style.top = Math.sin(ang) * r + clockWidth / 2 - fontSize * numberRadiusPercent /2;
		e.style.left = Math.cos(ang) * r + clockHeight / 2 - fontSize * numberRadiusPercent /2;

		e.innerHTML = i;
		Menu.setStyle(e.style, ss.hoursNumbers.style);
		
		f.appendChild(e);
	}
	return f;
}
	
function animate () {
	
	let t, date = new Date(),
	h = date.getHours(),
	m = date.getMinutes(),
	s = date.getSeconds()
	;
	
	
/*
	hour = hour % 12 // da 12 postane 0
	hour = hour * PI /6		// krug je 2PI / 12 za svaki sat
	+ min * PI/(6*60)		// 12-i deo kruga + 60-i deo od toga za min
	+ sec * PI/ (6*60 * 60)	// 12.deo kruga + 60i deo za min + 60 deo za sec
	
	min * Math.PI / 30	=== samo ugao za minute 2PI/60

	min * Math.PI/30
	+ sec * Math.PI/ (6*60)	
	
	sec * Math.PI*2 / 60	samo ugao za sekunde
*/
	// h * stepeni = ugao kazaljke	360/24 = 15 i 360/12 = 30
	let r = twentyFour == 24 ? h*15 : h *30;
	t = 'rotate('+ r +'deg)';
//	console.log(t);
	hoursArm.style.webkitTransform = t; 
    hoursArm.style.mozTransform    = t;
    hoursArm.style.msTransform     = t; 
    hoursArm.style.oTransform      = t; 
    hoursArm.style.transform       = t; 

	t = 'rotate('+ m * 6 +'deg)';		// 360 stepeni / 60 minuta na satu = 6
//	console.log(t);
	minutesArm.style.webkitTransform = t; 
    minutesArm.style.mozTransform    = t; 
    minutesArm.style.msTransform     = t; 
    minutesArm.style.oTransform      = t; 
    minutesArm.style.transform       = t; 

	t = 'rotate('+ s * 6 +'deg)';	// + s * Math.PI/30
//	console.log(t);
	secondsArm.style.webkitTransform = t; 
    secondsArm.style.mozTransform    = t; 
    secondsArm.style.msTransform     = t; 
    secondsArm.style.oTransform      = t; 
    secondsArm.style.transform       = t; 
	
//console.log(h + ' ' + m + ' ' + s);
}


init();
}

