function BaseCodeGame (outerElem) {

	if (!outerElem || typeof outerElem !== 'object') {
		console.log('No outer elem: '+ outerElem);	
		return false;
	}	
	

const container = document.createElement('DIV');
const menu = new Menu(container);
const boardElem = document.createElement('DIV');

function init () {

	container.style.userSelect = 'none';
	container.style.overflow = 'hidden';
	container.style.position = 'relative';
	container.style.minWidth = '320px';
	container.style.minHeight = '320px';
	container.style.width = '100%';
	container.style.height = '100%';
	container.style.display = 'flex';
	container.style.flexFlow = 'column nowrap';



	boardElem.style.userSelect = 'none';
	boardElem.style.overflow = 'hidden';
	boardElem.style.position = 'relative';
	boardElem.style.flexGrow = 1;
	boardElem.style.backgroundColor = '#fbff0270';

	container.appendChild(boardElem);

	outerElem.innerHTML = '';
	outerElem.appendChild(container);
}// ---------------------- end init () -----------------------

function resizeBoard () {

	let sizeMin = getSquareSize();

	for (let row = 0; row < boardRows; ++row)
	{
		for (let col = 0; col < boardCols; ++col)
		{
			let div = squares[row][col];
			div.style.width = sizeMin +'px';
			div.style.height = sizeMin +'px';
			div = null;
		}
	}
}

function createBoard () {

	boardElem.innerHTML = '';
	squares.length = 0;

	let frag = document.createDocumentFragment();

	getBoardSize();

	let sizeMin = getSquareSize();


	for (let row = 0; row < boardRows; ++row)
	{
		squares[row] = [];
		let rowElem = document.createElement('DIV');
		rowElem.style.display = 'flex';
		rowElem.style.flexFlow = 'row nowrap';

		for (let col = 0; col < boardCols; ++col)
		{
			let div = document.createElement('DIV');
			div.innerHTML = row * boardCols + col;
			div.style.boxSizing = 'border-box';
			div.style.width = sizeMin + 'px';
			div.style.height = sizeMin + 'px';
			div.style.display = 'flex';
			div.style.justifyContent = 'center';
			div.style.alignItems = 'center';
			div.style.border = '1px solid red';
			div.style.position = 'relative';
			div.style.overflow = 'hidden';
			squares[row][col] = div;
			rowElem.append(div);
			div = null;	
		}

		frag.append(rowElem);
		rowElem = null;
	}

	boardElem.appendChild(frag);

frag = null;
}



function Menu (container) {

	const menuElem = document.createElement('DIV');

	const fullScreenButton = new gl.FullScreenButton(container);

	const volumeElem = document.createElement('input');

function init () {

	var wrap;

	menuElem.style.userSelect = 'none';
	menuElem.style.overflow = 'hidden';
	menuElem.style.position = 'relative';
	menuElem.style.display = 'flex';
	menuElem.style.justifyContent = 'space-around';
	menuElem.style.padding = '0.5em 0';
	menuElem.style.backgroundColor = '#ccc';
//	menuElem.innerHTML = 'menu';

	wrap = document.createElement('DIV');
	wrap.style.display = 'flex';
	wrap.style.flex = 1;
	wrap.style.justifyContent = 'space-around';



	// FULL SCREEN
	wrap.appendChild(fullScreenButton.getElem());


	volumeElem.type = 'range';
	volumeElem.style.width = '50%';
	volumeElem.max = 100;
	volumeElem.min = 0;
	volumeElem.value = 100;
	volumeElem.addEventListener('input', function (e) {
	
		let vol = parseInt(e.target.value, 10) / 100;

		for (let card of cards)
		{
			card.setVolume(vol);
		}
	});

	wrap.appendChild(volumeElem);

	menuElem.appendChild(wrap);
	container.appendChild(menuElem);

	wrap = null;
}
init();
}// ------------------------------- end Menu()----------------------------------


init();
} // ------------------ end BaseGame ()-------------------------

const gl = new GameLib();


let test = new SnakeGrid();
let g8 = new SnakeGrid(gl.getFirstEmptyElem('.your_game_here'), gl);
let g9 = new SnakeSliding(gl.getFirstEmptyElem('.your_game_here'), gl);
let g7 = new AnalogClock(gl.getFirstEmptyElem('.your_game_here'), 24, true, gl);
let g6 = new FourInLine(gl.getFirstEmptyElem('.your_game_here'), gl);
//let g5 = new Tarot(gl.getFirstEmptyElem('.your_game_here'));
let g4 = new CanvasPaint(gl.getFirstEmptyElem('.your_game_here'), gl);
let g3 = new WackAMole (gl.getFirstEmptyElem('.your_game_here'), gl);
let g2 = new WackAGhost (gl.getFirstEmptyElem('.your_game_here'), gl);
let g1 = new MemoryGame(gl.getFirstEmptyElem('.your_game_here'), gl);