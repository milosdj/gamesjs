
function SnakeSliding (outerElem, gl) {
'use strict';
	if (!outerElem || typeof outerElem !== 'object') {
		return false;
	}	
	
let gaming = false;
let boardCols = 10;
let boardRows = 9;
let totalSquares = boardCols * boardRows;
let boardHeight = 0;
let boardWidth = 0;
let squareSize = 0;

const squares = [];
const snakeArr = []	//new gl.NodeChain();
let snakeSpeed = 10;
let smooth = false;
let keyDirection = 'u';	
// let direction = {x: 0, y: -snakeSpeed};	// udlr
const keys = [];		// new gl.NodeChain();	// remember keys
let skipTime = 0;
let oldTime = 0;

let headColor = 'black';
let foodColor = gl.rndRGBlh(4,8);


function SnakeCircle (row, col, r, color) {
	this.row = row;
	this.col = col;
	let x = (col-1) * r;
	let y = (row-1) * r;
	let size = r;
	this.color = color;
	let currentDir = ''	// udlr or 1 2 3 4

	const elem = document.createElement('DIV');
	elem.style.width = size;
	elem.style.height = size// + 'px';
	elem.style.borderRadius = '50%';
	elem.style.position = 'absolute';
	elem.style.zIndex = 2000;
	elem.style.backgroundColor = color;
	elem.style.top = Math.floor(y);
	elem.style.left = Math.floor(x);

	this.elem = elem;

function getLastDir () {
	let dir = snakeArr[snakeArr.length -1].currentDir;
	return
}

this.move = function (dt) {

	let moveX = x + direction.x * dt;
	if (moveX < 0 || boardCols * squareSize < moveX + squareSize )	die();
	else x = moveX;	// speed

	let moveY = y + direction.y * dt;
	if (moveY < 0 || boardRows * squareSize < moveY + squareSize)	die();
	else y = moveY;	// speed

	elem.style.top = Math.floor(y);
	elem.style.left = Math.floor(x);

};

this.setDirection = function () {



}



}// ------------------------ end SnakeCircle -----------------------------------


const container = document.createElement('DIV');
const menu = new Menu(container);
const boardElem = document.createElement('DIV');

function init () {

	container.style.userSelect = 'none';
	container.style.overflow = 'hidden';
	container.style.position = 'relative';
	container.style.minWidth = '320px';
	container.style.minHeight = '320px';
	container.style.width = '100%';
	container.style.height = '100%';
	container.style.display = 'flex';
	container.style.flexFlow = 'column nowrap';

	boardElem.tabIndex = 0;
	boardElem.style.boxSizing = 'border-box';
	boardElem.style.padding = 0;
	boardElem.style.userSelect = 'none';
	boardElem.style.overflow = 'hidden';
	boardElem.style.position = 'relative';
	boardElem.style.flexGrow = 1;
	boardElem.style.backgroundColor = '#fbff0270';
	boardElem.style.display = 'flex';
	boardElem.style.flexFlow = 'column nowrap';
	boardElem.style.justifyContent = 'flex-start';
	boardElem.style.alignItems = 'flex-start';
	boardElem.style.alignContent = 'center';
	boardElem.addEventListener('keydown', onKeyDown);

	boardElem.addEventListener('keyup', onKeyUp);


	container.appendChild(boardElem);


	outerElem.innerHTML = '';
	outerElem.appendChild(container);

	getBoardSize();	// must be after container goes to dom

	


	window.addEventListener('resize', function () {
	
		if (boardHeight != boardElem.clientHeight
			|| boardWidth != boardElem.clientWidth)
		{
			getBoardSize();
		
		//	resizeBoard();	
		}
	});


	// must be avter container goes to dom
//	createBoard();




}// ---------------------------- end init () -----------------------------------

function onKeyDown (e) {

	let dir = getUDLR(e.key || e.which || e.keyCode);

	if (keys.indexOf(dir) >= 0)
	{
		e.preventDefault();	
		return;
	}
	
	keys.push(dir);
	keyDirection = dir;

//	setDirection(dir);

	// block all other keys Esc, F5...
	e.preventDefault();
}

function onKeyUp (e) {

	let dir = getUDLR(e.key || e.which || e.keyCode);

	let index = keys.indexOf(dir);
	if (index < 0)
	{
		e.preventDefault();		// block tab, F5, Esc
		return;					
	}
	keys.splice(index, 1) ;

	let last = keys.length -1;
	if (keys[last])	 keyDirection = keys[last];


//	setDirection(dir);

	e.preventDefault();
}

function setDirection (udlr) {

	switch (udlr)
	{
	case 'u':
		direction.x = 0;
		direction.y = -snakeSpeed;
	break;

	case 'd':
		direction.x = 0;
		direction.y = snakeSpeed;
	break;

	case 'l':
		direction.x = -snakeSpeed;
		direction.y = 0;
	break;

	case 'r':
		direction.x = snakeSpeed;
		direction.y = 0;
	break;

	default:		
	}

}
// return u d l r 
function getUDLR (code) {

		switch (code)
		{
		case 38:	//	up arrow
		case 'ArrowUp':
		case 87:	//	W
		case 119:	//	w
		case 'w':
			return 'u';

		case 40:	// down arrow
		case 'ArrowDown':
		case 83:	// S
		case 115:	// s
		case 's':
			return 'd';
	
		case 37:	// left arrow
		case 'ArrowLeft':
		case 65:	// A
		case 97:	// a
		case 'a':
			return 'l';
	
		case 39:	// right arrow
		case 'ArrowRight':
		case 68:	// D
		case 100:	// d
		case 'd':
			return 'r';

		default:
		return '';
		}
}

function getBoardSize () {

	boardHeight = boardElem.clientHeight;
	boardWidth = boardElem.clientWidth;
	squareSize = getSquareSize(); 
}
function getSquareSize () {

	let space = 0;
	let h = Math.floor(boardHeight / boardRows) - space;
	let w = Math.floor(boardWidth / boardCols) - space;

	return w < h ? w : h;
}
function resizeBoard () {

	for (let row = 0; row < boardRows; ++row)
	{
		for (let col = 0; col < boardCols; ++col)
		{
			let div = squares[row][col];
			div.style.width = squareSize;
			div.style.height = squareSize;
			div = null;
		}
	}
}
function createBoard () {

	boardElem.innerHTML = '';
	squares.length = 0;

	let frag = document.createDocumentFragment();

	getBoardSize();

	let sizeMin = getSquareSize();


	for (let row = 0; row < boardRows; ++row)
	{
		squares[row] = [];
		let rowElem = document.createElement('DIV');
		rowElem.style.display = 'flex';
		rowElem.style.flexFlow = 'row nowrap';

		for (let col = 0; col < boardCols; ++col)
		{
			let div = document.createElement('DIV');
			div.innerHTML = row * boardCols + col;
			div.style.boxSizing = 'border-box';
			div.style.width = sizeMin + 'px';
			div.style.height = sizeMin + 'px';
			div.style.display = 'flex';
			div.style.justifyContent = 'center';
			div.style.alignItems = 'center';
			div.style.border = '1px solid red';
			div.style.position = 'relative';
			div.style.overflow = 'hidden';
			squares[row][col] = div;
			rowElem.append(div);
			div = null;	
		}

		frag.append(rowElem);
		rowElem = null;
	}

	boardElem.appendChild(frag);

frag = null;
}


function pauseGame () {

	menu.startButtonTxt('Play');
	gaming = false;
//	cancelAnimationFrame(rafID);
}
function playGame () {

	menu.startButtonTxt('Pause')
	gaming = true;
//	console.log(performance.now());
	requestAnimationFrame(animate);
}
function startGame () {

	menu.startButtonTxt('Pause');
	gaming = true;
	snakeArr.length = 0;
	let part = new SnakeCircle(Math.floor(boardRows/2), Math.floor(boardCols/2), squareSize, headColor);
	snakeArr.push(part);
	boardElem.innerHTML = '';
	boardElem.append(part.elem);

	boardElem.focus();
	requestAnimationFrame(animate);
}


function animate (time) {
	if (!gaming)	return;
	if (oldTime <= 0)	oldTime = time;
	let dt = (time - oldTime) /1000;		// delta time
	oldTime = time;
//	console.log(dt);

//	skipTime += dt;
//	if (skipTime < 1000) {
//		requestAnimationFrame(animate);
//		return;
//	}
//	skipTime = 0;
//	console.log(skipTime);

	moveSnake(dt);

	requestAnimationFrame(animate);
}

function moveSnake (dt) {

	let head = snakeArr[0];

	for (let i = 0, len = snakeArr.length; i < len; ++i)
	{
		snakeArr[i].move(dt);
	}

}



function die () {

	pauseGame();
	console.log('died:');

}





function Menu (container) {

	let fontSize = 20;
	const menuElem = document.createElement('DIV');

	const fullScreenButton = new gl.FullScreenButton(container);
	const startButtonElem = document.createElement('BUTTON');
	const smoothElem = document.createElement('INPUT');
	const boardSizeElem = document.createElement('input');
	const displayBoardSizeElem = document.createElement('DIV');

function init () {

	var wrap, div;

	menuElem.style.userSelect = 'none';
	menuElem.style.overflow = 'hidden';
	menuElem.style.position = 'relative';
	menuElem.style.display = 'flex';
	menuElem.style.justifyContent = 'space-around';
	menuElem.style.padding = '5px';
	menuElem.style.backgroundColor = '#ccc';
	menuElem.style.fontSize = fontSize;
//	menuElem.innerHTML = 'menu';

	wrap = document.createElement('DIV');
	wrap.style.display = 'flex';
	wrap.style.flex = 1;
	wrap.style.justifyContent = 'space-around';

	// FULL SCREEN
	wrap.appendChild(fullScreenButton.getElem());

	// START BUTTON
	startButtonElem.innerHTML = 'Start';
	startButtonElem.style.fontSize = 'inherit';
	startButtonElem.addEventListener('click', function (e) {
	
		if (!gaming && this.innerHTML === 'Start')
		{
			startGame();
			return;
		}

		if (gaming)		pauseGame();
		else			playGame();
	});
	wrap.appendChild(startButtonElem);







	div = document.createElement('LABEL');
	div.fontSize = 'inherit';
	div.innerHTML = 'Smooth: ';
	div.style.display = 'flex';
	div.style.alignItems = 'center';
	smoothElem.type = 'checkbox';
	smoothElem.addEventListener('input', function (e) {
	
		smooth = !!e.target.checked;
	});
	div.append(smoothElem);
//	div.style.border = '1px solid red';
	wrap.append(div)
	menuElem.appendChild(wrap);


	wrap = document.createElement('DIV');
	wrap.style.display = 'flex';
	wrap.style.flex = 1;
	wrap.style.flexFlow = 'column nowrap';
	wrap.style.justifyContent = 'space-around';

	// DISPLAY SIZE
	displayBoardSizeElem.innerHTML = boardRows + 'x' + boardCols + ' = ' + totalSquares;
	wrap.appendChild(displayBoardSizeElem);

	// SIZE
	boardSizeElem.type = 'range';
	boardSizeElem.style.width = '100%';
	boardSizeElem.max = 30;
	boardSizeElem.min = 4;
	boardSizeElem.value = 9;
	boardSizeElem.addEventListener('input', function (e) {
	
		boardRows = parseInt(e.target.value, 10);
		boardCols = boardRows + 1;
		totalSquares = boardCols * boardRows;

		displayBoardSizeElem.innerHTML = boardRows + 'x' + boardCols + ' = ' + totalSquares;

		createBoard();
	})

	wrap.appendChild(boardSizeElem);

	menuElem.appendChild(wrap);
	container.appendChild(menuElem);

	wrap = div = null;
} // ----------- end init()-------------


this.startButtonTxt = function (str) {

	startButtonElem.innerHTML = str;
};


init();
}// ------------------------------- end Menu()----------------------------------


init();
} // ------------------ end Snake ()-------------------------
