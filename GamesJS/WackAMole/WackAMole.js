
/*
https://openclipart.org/detail/168496/cartoon-mole-by-magnesus

http://soundbible.com/993-Upper-Cut.html
http://soundbible.com/991-Left-Hook.html
http://soundbible.com/990-Right-Hook.html
http://soundbible.com/546-Fish-Splashing.html
http://soundbible.com/1295-Dying-Light-Bulb.html
http://soundbible.com/1148-Bubbling.html

https://www.freepik.com/free-vector/grass-shape-background-design_893901.htm

*/
function WackAMole (outerElem, gl) {
'use strict';
	if (!outerElem || typeof outerElem !== 'object')
	{
		console.log('No outer elem: '+ outerElem);	
		return false;
	}

const audio = new gl.Sound(0.1);
const container = document.createElement('DIV');
const menu = new Menu(); 
const boardElem = document.createElement('DIV');
const holes = [];
const moles = [];
let boardRows = 3;
let boardCols = 3;
const imgPath = 'WackAMole/img/mole.svg';		//'url(img/mole.svg)';
const moleImg = new Image();
// new Image(); moleImg.src = 'img/mole.svg';

let score = 0;
let moleCounter = 0;
let level = 3;
let gameOn = false;
let rafId;
let time = {
level: level * 60000,
gameTotalMS:0,
gameLastMS:0,
startFrame:0,
lastFrame:0,
deltaFrame:0,	
};
function resetTime (l = 60000) {

	time.level = l;
	time.gameTotalMS = 0;
	time.gameLastMS = 0;
	time.startFrame = 0;
	time.lastFrame = 0;
	time.deltaFrame = 0;
}
function init () {

	audio.i('punch1').src('WackAMole/snd/Left Hook-SoundBible.com-516660386.mp3');
	audio.i('punch2').src('WackAMole/snd/Right Hook-SoundBible.com-1406389182.mp3');
	audio.i('punch3').src('WackAMole/snd/Upper Cut-SoundBible.com-1272257235.mp3');
	audio.i('hit1').src('WackAMole/snd/Blood Splattering On Wall-SoundBible.com-508930244.mp3');
	audio.i('hit2').src('WackAMole/snd/Blood Splatters-SoundBible.com-125814492.mp3');
	audio.i('hit3').src('WackAMole/snd/Slime-SoundBible.com-803762203.mp3');
	audio.i('hit4').src('WackAMole/snd/Squishy 2-SoundBible.com-1775292371.mp3');
	audio.i('miss').src('WackAMole/snd/Mirror Shattering-SoundBible.com-1752328245.mp3');
	audio.i('laugh1').src('WackAMole/snd/Commedy_Punch-Poorna_RAo-1017287436.mp3');
	audio.i('laugh2').src('WackAMole/snd/Kid Giggle 2-SoundBible.com-847618472.mp3');

	container.style.userSelect = 'none';
	container.style.overflow = 'hidden';
	container.style.position = 'relative';
	container.style.minWidth = '320px';
	container.style.minHeight = '320px';
	container.style.width = '100%';
	container.style.height = '100%';
	container.style.display = 'flex';
	container.style.flexFlow = 'column nowrap';
	container.style.borderRadius = '0.5rem';
	container.style.alignItems = 'stretch';
	container.style.backgroundColor = 'white';
	container.style.backgroundImage =		'url(WackAMole/img/grass.png)';
	container.style.backgroundRepeat =		'repeat';
	container.style.backgroundPosition =	'center center';
	container.style.backgroundSize =		'30%';


	boardElem.style.display = 'flex';
	boardElem.style.flexFlow = 'column nowrap';
	boardElem.style.flexGrow = 1;
	boardElem.style.boxSizing = 'border-box';
	boardElem.style.userSelect = 'none';
	boardElem.style.overflow = 'hidden';
	boardElem.position = 'relative';
	boardElem.style.justifyContent = 'center';
	boardElem.style.alignContent = 'center';
//	boardElem.style.backgroundColor = '#fbff0270';
	boardElem.style.borderRadius = '0.5rem';
	boardElem.addEventListener('mousedown', boardMouseDown);
	createBoard();
	container.appendChild(boardElem);

	outerElem.innerHTML = '';
	outerElem.appendChild(container);


}// ---------------------- end init () -----------------------

function createBoard () {

	let frag = document.createDocumentFragment(), row, div;

	moles.length = 0;
	holes.length = 0;
	boardElem.innerHTML = '';

	for (let r = 0; r < boardRows; ++r)
	{
		row = document.createElement('DIV');
		row.style.display = 'flex';
		row.style.flex = 1;
		row.style.overflow = 'hidden';
		row.style.height = '30%';
//		row.style.border = '1px solid blue';

		for (let c = 0; c < boardCols; ++c)
		{
			div = document.createElement('DIV');
			div.setAttribute('data-id', r * boardRows + c);
			div.style.userSelect = 'none';
			div.style.overflow = 'hidden';
			div.style.position = 'relative';
			div.style.width =		'100%';
			div.style.maxWidth =	'100%';
			div.style.height =		'100%';
			div.style.maxHeight =	'100%';
			div.style.borderRadius = '0.5rem';
			div.style.border = '1px solid darkgray';
	//		div.style.backgroundRepeat =	'no-repeat';
	//		div.style.backgroundPosition =	'center center';
	//		div.style.backgroundSize =		'contain';

			holes.push(div);
			row.append(div);
		}

		frag.append(row);
		row = document.createElement('DIV');
	}

	boardElem.appendChild(frag);

	for (let i = 0; i < level; ++i)
	{
		moles.push(new Mole());
	}


frag = row = div = null;
}

function boardMouseDown (e) {
	if (!gameOn)	return;

	let div = e.target;
	//console.log(div);
	while (div.tagName === 'IMG')	div = div.parentNode;

	let id = div.getAttribute('data-id');
	id = parseInt(id, 10);	
	if (!(0 <= id && id <= 8))	return;
	audio.i('punch1').play();

let hit = false;

//console.log(id);
	for (let i = moles.length - 1; i >= 0; --i)
	{
		if (moles[i].isHit(id))
		{
			audio.i('hit1').play();

			hit = true;
			++score;
			menu.displayScore();
			time.level += 1000;	// 2 sec per killed mole
			++moleCounter;
			if (moleCounter >= level * 20)
			{
				moleCounter = 0;
				levelUp();
			}
			holes[id].innerHTML = '';
			moles[i].randomMole();		
		}
	}

	if (!hit)
	{
		audio.i('miss').play();
		--score;
		menu.displayScore();
		if (score < -10)	gameOver();

		time.level -= 10000;
	}


div = id = null;
}

function countDown (t) {
	if (!time.startFrame)	time.startFrame = t;
	if (!time.lastFrame)	time.lastFrame = t;
	if (!time.gameLastMS)	time.gameLastMS = t - time.startFrame;
	time.deltaFrame =  t - time.lastFrame;		// Math.round( * 1000) / 1000;
	time.gameTotalMS = t - time.startFrame;
	time.lastFrame = t;

	time.level -= time.deltaFrame;
	menu.displayTime();




	for (let i = moles.length-1; i >= 0; --i)
	{
		moles[i].animate(time.deltaFrame);
	}


	if (time.level <= 0)
	{
		gameOver();
		return;
	}
rafId = requestAnimationFrame(countDown);
}

function clearHoles () {

	for (let i = holes.length-1; i >= 0; --i)
	{
		holes[i].innerHTML = '';
	}
}



function gameOver () {

	cancelAnimationFrame(rafId);
	gameOn = false;
	menu.displayTime();
	menu.displayScore();

	let str = '';

	if (score > 0)
	{
		str = `Score: ${score}    Level: ${level}

	Well done :)`;
	}
	else if (score < 0)
	{
		str = `How did you do that? NEGATIVE score?!?

	You must be something special :)

	Score: ${score}  Level: ${level} Time left: ${Math.floor(time.level/1000)}`;
	}
	else
	{
		str = `Zero score! Really?
	That took some effort ;)`;
	}
	gl.msgBox(str);

	resetGame();
}

function levelUp () {

	cancelAnimationFrame(rafId);
	gameOn = false;

	let str = `Score: ${score}    Level: ${level}
Well done :)

Ok for next Level!
Cancel to stay on same ground.`;

	gl.msgBox(str, 'okc', function (r){
	
		if (r)
		{
			++level;
			moles.push(new Mole());
		}
		nextLevel(level);

	});

}


function nextLevel (l=1) {
	clearHoles();
	gameOn = true;
	resetTime(time.level + l * 10000);
	rafId = requestAnimationFrame(countDown);
}

function resetGame () {

	cancelAnimationFrame(rafId);
	clearHoles();
	moles.length = 0;
	moles.push(new Mole());
	gameOn = false;
	moleCounter = 0;
	score = 0;
	menu.displayScore(score);
	level = 1;
	resetTime(level * 60000);
	menu.displayTime();
	menu.setStartButton('start');
}


function Mole () {

let pos;
let downTime;
let upTime;
let digUpTimeTotal = 1000;
let digUpTime = digUpTimeTotal;
let digDownTimeTotal = 1000;
let digDownTime = digDownTimeTotal;

let inHole = false;
let imgElem = new Image();

function init () {

	imgElem.src = imgPath;
	imgElem.setAttribute('draggable', 'false');
	imgElem.setAttribute('user-select', 'none');
	imgElem.ondragstart = function () {return false;};
	imgElem.style.opacity = 0;
	imgElem.style.position = 'absolute';
	imgElem.style.top = '1rem';
	imgElem.style.left = '1rem';

	randomMole();
}

this.isHit = function (p) {

	return inHole && pos === p;
};

this.animate = animate;
function animate (t) {

	if (downTime > 0)
	{
		downTime -= t;
		return;
	}

	if (!inHole) 
	{
		// dig up
		inHole = true;
		imgElem.style.opacity = 0;
		holes[pos].appendChild(imgElem);
		return;
	}

	if (digUpTime > 0)
	{
		digUpTime -= t;
		imgElem.style.opacity = 1 - (digUpTime / Math.max(200, digUpTimeTotal - level * 100));
		return;
	}


	if (upTime > 0)
	{
		upTime -= t;
		return;
	}
	// dig.down

	if (digDownTime > 0)
	{
		digDownTime -= t;
		imgElem.style.opacity = digDownTime / Math.max(200, digDownTimeTotal - level * 100);
		return;
	}
	audio.i('laugh1').play();


	inHole = false;
	holes[pos].innerHTML = '';
	--score;
	menu.displayScore();
	time.level -= 10000;
	randomMole();	
}


this.randomMole = randomMole;
function randomMole () {

	inHole = false;
	pos = Math.floor(Math.random() * holes.length);
	downTime = Math.max(200, (Math.random() * 1000 - level * 100));
	upTime = Math.max(200, (Math.random() * 3000 + 2000 - (level * 1000)));

	digUpTime = Math.max(200, digUpTimeTotal - level * 100);
	digDownTime = Math.max(200, digDownTimeTotal - level * 100);
	imgElem.style.opacity = 0;
}


init();
}


function Menu () {
	
	let fontSize = 16;

	const menuElem = document.createElement('DIV');

	const scoreElem = document.createElement('DIV');
	const timeLeftElem = document.createElement('DIV');
	const startButton = document.createElement('BUTTON');
	const soundElem = document.createElement('INPUT');
	const menu = this;
	const fsButton = new gl.FullScreenButton(container);

function init () {

	var wrap, div, label;

	menuElem.style.fontSize = fontSize;
	menuElem.style.userSelect = 'none';
	menuElem.style.overflow = 'hidden';
	menuElem.style.position = 'relative';
	menuElem.style.display = 'flex';
	menuElem.style.flexFlow = 'row nowrap';
	menuElem.style.justifyContent = 'space-between';
	menuElem.style.padding = '3px';
	menuElem.style.backgroundColor = '#00000040';

	wrap = document.createElement('DIV');
	wrap.style.display = 'flex';
	wrap.style.flex = 1;
	wrap.style.justifyContent = 'space-between';

	// FULL SCREEN BUTTON
	wrap.append(fsButton.getElem());
	menuElem.append(wrap);


	// START BUTTON
	startButton.innerHTML = 'Start';
	startButton.style.display = 'flex';
//	startButton.style.flex = 1;
	startButton.style.justifyContent = 'center';
	startButton.style.alignItems = 'center';
//	startButton.style.marginLeft = '1rem';
//	startButton.style.border = '1px solid green';
	startButton.style.borderRadius = '0.3rem';
	startButton.style.backgroundColor = '#ffb071';
	startButton.style.fontSize = 'inherit';
	startButton.addEventListener('click', onStart);

	wrap.appendChild(startButton);
	menuElem.appendChild(wrap);


	// SOUND ON OFF
	soundElem.type = 'checkbox';
	soundElem.checked = 'checked';
	soundElem.addEventListener('change', onSoundChange);
	label = document.createElement('LABEL');
	label.style.display = 'flex';
	label.style.justifyContent = 'center';
	label.style.alignItems = 'center';
	label.style.padding = '0 0.5rem';
	label.style.border = '1px solid darkgray';
	label.style.borderRadius = '0.3rem';
	label.style.backgroundColor = '#ddddddd0';
	label.innerHTML = 'Sound';
	label.style.fontSize = 'inherit';
	label.append(soundElem);
	wrap.appendChild(label);
	menuElem.appendChild(wrap);


	wrap = document.createElement('DIV');
	wrap.style.display = 'flex';
	wrap.style.flex = 2;
	wrap.style.justifyContent = 'space-around';
	wrap.style.backgroundColor = '#ddddddd0';
	wrap.style.borderRadius = '0.3rem';
//	wrap.style.marginLeft = '1rem';

	// TIME
	timeLeftElem.innerHTML = 60;
	timeLeftElem.style.textAlign = 'center';
	timeLeftElem.style.fontSize = '150%';
	div = document.createElement('DIV');
	div.style.padding = '0 0.5rem';
	div.innerHTML = 'Time:';
	div.appendChild(timeLeftElem);
	wrap.appendChild(div);

	// SCORE
	scoreElem.innerHTML = 0;
	scoreElem.style.textAlign = 'center';
	scoreElem.style.fontSize = '150%';
	div = document.createElement('DIV');
	div.style.padding = '0 0.5rem';
	div.innerHTML = 'Score:';
	div.appendChild(scoreElem);
	wrap.appendChild(div);

	menuElem.append(wrap);

	container.appendChild(menuElem);

	wrap = div = label = null;
}


function onStart () {

	if (gameOn)
	{
		gameOn = false;
		menu.setStartButton('start');
		cancelAnimationFrame(rafId);
		resetTime(time.level);
	}
	else
	{
		gameOn = true;
		menu.setStartButton('pause');
		//console.log(performance.now());
		rafId = requestAnimationFrame(countDown);
	}
}

function onSoundChange (e) {

	audio.setOn(e.target.checked);
}

this.setStartButton = function (s) {

	if (s.toUpperCase() === 'PAUSE')
	{
		startButton.innerHTML = 'Pause';
		startButton.style.backgroundColor = '#8eff00';
	}
	else
	{
		startButton.innerHTML = 'Start';
		startButton.style.backgroundColor = '#ff801c';	
	}
};

this.displayScore = function () {
	scoreElem.innerHTML = score;
};
this.displayTime = function () {
	timeLeftElem.innerHTML = Math.floor(time.level / 1000) ;
};
init();
} // end Menu()

init();
} // ------------------ end BaseGame ()-------------------------
