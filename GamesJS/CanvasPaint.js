

function CanvasPaint (outerElem) {
'use strict';

	if (!outerElem || typeof outerElem !== 'object')
	{
		console.log('No outer elem: '+ outerElem);	
		return false;
	}

const container = document.createElement('DIV');
const menu = new Menu(container);

const canvas = document.createElement('CANVAS');
const ctx = canvas.getContext('2d');
let imageData;

const ctxStyle = {
	strokeStyle : '#000000',	// line color
	lineJoin : 'round',
	lineCap : 'round',
	lineWidth : 1,
};

let paint = false;
let touchPositions = [];
let lastMousePosition = {x:0,y:0};
let keyDown = {space: false};

function init () {
	window.addEventListener('resize', onResize);

	canvas.style.display = 'flex';
	canvas.style.flex = 1;
//	canvas.style.height = '100%';
//	canvas.style.width = '100%';
	canvas.style.overflow = 'hidden';
	canvas.style.boxSizing = 'border-box';
	canvas.style.backgroundColor = 'white';
	canvas.addEventListener('mousedown', function (e)
	{
		paint = true;
		lastMousePosition.x = e.offsetX;
		lastMousePosition.y = e.offsetY;	
	});
	canvas.addEventListener('mouseup', function (e)
	{
		canvasMouseMove(e);
		paint = false;
	});
	canvas.addEventListener('mouseout', function (e)
	{
		canvasMouseMove(e);
		paint = false;
	});
	canvas.addEventListener('mousemove', canvasMouseMove);
	canvas.addEventListener('mouseover', function(e)	// continue paint
	{
		if (e.buttons > 0 || e.which > 0)
		{
			paint = true;
			lastMousePosition.x = e.offsetX;
			lastMousePosition.y = e.offsetY;	
		}
	});

	canvas.addEventListener('touchstart', function (e)
	{
		paint = true;
		touchPositions = getFingerPositions(e);

	});
	canvas.addEventListener('touchcancel', function (e)
	{
		canvasTouchMove(e);
		paint = false;
	});
	canvas.addEventListener('touchend', function (e)
	{
		canvasTouchMove(e);
		paint = false;
	});
	canvas.addEventListener('touchmove', canvasTouchMove);

	container.tabIndex = 0;
	container.style.boxSizing = 'border-box';
	container.style.userSelect = 'none';
	container.style.overflow = 'hidden';
	container.style.position = 'relative';
	container.style.minWidth = '320px';
	container.style.minHeight = '320px';
	container.style.width = '100%';
	container.style.height = '100%';
	container.style.display = 'flex';
	container.style.flexFlow = 'column nowrap';
	container.style.border = '1px solid darkgray';
	container.appendChild(canvas);


	container.addEventListener('keydown', function (e)
	{
		if (e.keyCode === 32)
		{
			keyDown.space = true;
			e.preventDefault();
			e.stopPropagation();
		}
	});
	container.addEventListener('keyup', function (e)
	{
		if (e.keyCode === 32)
		{
			keyDown.space = false;
		}
	});
/*	container.addEventListener('fullscreenchange', () => {
		canvas.width = canvas.clientWidth;
		canvas.height = canvas.clientHeight;
		setStyle();
	});
*/
	outerElem.innerHTML = '';
	outerElem.appendChild(container);


	canvas.width = canvas.clientWidth;
	canvas.height = canvas.clientHeight;

	setStyle();

}// ----------------------- end init () ---------------------------------

function setStyle () {

	for (let s in ctxStyle) { ctx[s] = ctxStyle[s]; }
}


function canvasMouseMove (e) {

	if (!paint)	return;

	ctx.beginPath();
	ctx.moveTo(lastMousePosition.x, lastMousePosition.y);
	ctx.lineTo(e.offsetX, e.offsetY);
	ctx.closePath();
	ctx.stroke();

	if (!keyDown.space)
	{
		lastMousePosition.x = e.offsetX;
		lastMousePosition.y = e.offsetY;
	}
}

function canvasTouchMove (e) {

	if (!paint)	return;

	let pos = getFingerPositions(e);
//	alert(pos.length);
	ctx.beginPath();
	for (let i = 0; i < pos.length; ++i)
	{

		ctx.moveTo(touchPositions[i].x, touchPositions[i].y);
		
		ctx.lineTo(pos[i].x, pos[i].y);

		touchPositions[i].x = pos[i].x;
		touchPositions[i].y = pos[i].y;
		touchPositions[i].force = pos[i].force;

	}
	ctx.closePath();
	ctx.stroke();

//	e.stopPropagation();
	e.preventDefault();
}

function getFingerPositions (e) {
	let ar=[];

//	if (e.changedTouches.length >= 2) alert('touches: ' + e.changedTouches.length);

	if (e.changedTouches)
	{
		let rect = e.target.getBoundingClientRect();
		for (let i = 0; i < e.changedTouches.length; ++i)
		{
			let o = {};
			let t = e.changedTouches[i];
			o.x = t.clientX - rect.left;
			o.y = t.clientY - rect.top;
			o.force = t.force;		// not working yet
			ar.push(o);
		}
	}
	return ar;
}


function onResize () {

	imageData = ctx.getImageData(0,0,canvas.clientWidth, canvas.clientHeight);

	canvas.width = canvas.clientWidth;
	canvas.height = canvas.clientHeight;
	setStyle();

	ctx.putImageData(imageData, 0, 0);
}


function Menu (container) {
	
	const menuElem = document.createElement('DIV');

	const colorElem = document.createElement('INPUT');
	const lineWidthElem = document.createElement('INPUT');
	const lineCapElem = document.createElement('SELECT');
	const lineJoinElem = document.createElement('SELECT');

	const fullScreen = new gl.FullScreenButton(container);

function init () {

	var wrap, option;

	menuElem.style.userSelect = 'none';
	menuElem.style.overflow = 'hidden';
	menuElem.style.position = 'relative';
	menuElem.style.display = 'flex';
	menuElem.style.justifyContent = 'space-around';
	menuElem.style.padding = '0.5em';
	menuElem.style.backgroundColor = '#b2b2b2';
//	menuElem.style.border = '1px solid red';
	menuElem.addEventListener('mouseout', () => {container.focus();});	// for key space


	// FULL SCREEN
	menuElem.appendChild(fullScreen.getElem());

	wrap = document.createElement('DIV');
	wrap.style.display = 'flex';
	wrap.style.flex = 4;
	wrap.style.justifyContent = 'space-around';
	wrap.style.alignItems = 'center';
//	wrap.style.border = '1px solid red';



	colorElem.type = 'color';
//	colorElem.style.height = '100%';
	colorElem.style.width = '3rem';
	colorElem.addEventListener('change', function (e) {

		let color = e.target.value || '#000000';
		ctxStyle.strokeStyle = color;
		ctx.strokeStyle = color;
	});
	wrap.appendChild(colorElem);


	// LINE CAP
	lineCapElem.style.display = 'inline-block';
//	lineCapElem.style.height = '100%';	// ubije canvas
	for (let i of ['round', 'butt', 'square'])
	{
		option = document.createElement('OPTION');
		option.value = i;
		option.text = i;
		lineCapElem.appendChild(option);
	}
	lineCapElem.addEventListener('change', function (e) {
	
		let cap = e.target.value || 'round';
		ctxStyle.lineCap = cap;
		ctx.lineCap = cap;
	});
	wrap.appendChild(lineCapElem);


	// LINE JOIN
	lineJoinElem.style.display = 'inline-block';
	//lineJoinElem.style.height = '100%';	// ubije canvas
	for (let i of ['round', 'mitter', 'beve'])
	{
		option = document.createElement('OPTION');
		option.value = i;
		option.text = i;
		lineJoinElem.appendChild(option);
	}
	lineJoinElem.addEventListener('change', function (e) {
	
		let join = e.target.value || 'round';
		ctxStyle.lineJoin = join;
		ctx.lineJoin = join;
	});

	wrap.appendChild(lineJoinElem);


	menuElem.appendChild(wrap);




	wrap = document.createElement('DIV');
	wrap.style.display = 'flex';
	wrap.style.flex = 4;
	wrap.style.justifyContent = 'space-around';

	lineWidthElem.type = 'range';
	lineWidthElem.style.width = '100%';
	lineWidthElem.value = 1;
	lineWidthElem.min = 1;
	lineWidthElem.max = 300;
	lineWidthElem.addEventListener('input', function (e) {

		let width = parseInt(e.target.value, 10) || 1;

		ctxStyle.lineWidth = width;
		ctx.lineWidth = width;
	});

	wrap.appendChild(lineWidthElem);
	menuElem.appendChild(wrap);

	container.appendChild(menuElem);

	wrap = option = null;
}





init();
}// ----------------------- end menu()-------------------------






init();
} //----------------------- end CanvasPaint () ---------------------------------

