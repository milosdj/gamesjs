
/*
https://openclipart.org/detail/168496/cartoon-mole-by-magnesus

http://soundbible.com/993-Upper-Cut.html
http://soundbible.com/991-Left-Hook.html
http://soundbible.com/990-Right-Hook.html
http://soundbible.com/546-Fish-Splashing.html
http://soundbible.com/1295-Dying-Light-Bulb.html
http://soundbible.com/1148-Bubbling.html

https://www.freepik.com/free-vector/grass-shape-background-design_893901.htm

*/
function WackAGhost (outerElem) {

	if (!outerElem || typeof outerElem !== 'object')
	{
		console.log('No outer elem: '+ outerElem);	
		return false;
	}	
	

const container = document.createElement('DIV');
const menu = new Menu(); 
const boardElem = document.createElement('DIV');
const holes = [];
const moles = [];
const imgPath = 'WackAMole/img/mole.svg';		//'url(img/mole.svg)';
const moleImg = new Image();
// new Image(); moleImg.src = 'img/mole.svg';

let score = 0;
let moleCounter = 0;
let level = 1;
let gameOn = false;
let rafId;
let time = {
level: level * 60000,
gameTotalMS:0,
gameLastMS:0,
startFrame:0,
lastFrame:0,
deltaFrame:0,	
};
function resetTime (l = 60000) {

	time.level = l;
	time.gameTotalMS = 0;
	time.gameLastMS = 0;
	time.startFrame = 0;
	time.lastFrame = 0;
	time.deltaFrame = 0;
}
function init () {



	container.style.userSelect = 'none';
	container.style.overflow = 'hidden';
	container.style.position = 'relative';
	container.style.minWidth = '120px';
	container.style.minHeight = '120px';
	container.style.width = '100%';
	container.style.height = '100%';
	container.style.display = 'flex';
	container.style.flexFlow = 'column nowrap';
	container.style.borderRadius = '1%';
	container.style.backgroundColor = 'white';
	container.style.backgroundImage =		'url(WackAMole/img/grass.png)';
	container.style.backgroundRepeat =		'repeat';
	container.style.backgroundPosition =	'center center';
	container.style.backgroundSize =		'30%';


	boardElem.style.userSelect = 'none';
	boardElem.style.overflow = 'hidden';
	boardElem.position = 'relative';
	boardElem.style.flexGrow = 1;
	boardElem.style.display = 'flex';
	boardElem.style.flexFlow = 'row wrap';
	boardElem.style.justifyContent = 'space-around';
	boardElem.style.alignContent = 'space-around';
//	boardElem.style.backgroundColor = '#fbff0270';
	boardElem.style.borderRadius = '1%';
	boardElem.addEventListener('mousedown', boardMouseDown);
	createBoard();

	container.appendChild(boardElem);

	outerElem.innerHTML = '';
	outerElem.appendChild(container);


}// ---------------------- end init () -----------------------

function createBoard () {

	let frag = document.createDocumentFragment();
	for (let i = 0; i < 9; ++i)
	{
		let div = document.createElement('DIV');
		div.setAttribute('data-id', i);
		div.style.userSelect = 'none';
		div.style.overflow = 'hidden';
		div.style.width =	'33%';
		div.style.height =	'33%';
		div.style.borderRadius = '2%';
		div.style.border = '1px solid darkgray';
//		div.style.backgroundRepeat =	'no-repeat';
//		div.style.backgroundPosition =	'center center';
//		div.style.backgroundSize =		'contain';

		holes.push(div);
		frag.appendChild(div);
		div = null;
	}

	boardElem.appendChild(frag);

	for (let i = 0; i < level; ++i)
	{
		moles.push(new Mole());
	}


frag = null;
}

function boardMouseDown (e) {
	if (!gameOn)	return;

	let div = e.target;
	//console.log(div);
	while (div.tagName === 'IMG')	div = div.parentNode;

	let id = div.getAttribute('data-id');
	id = parseInt(id, 10);	
	if (!(0 <= id && id <= 8))	return;

let hit = false;

//console.log(id);
	for (let i = moles.length - 1; i >= 0; --i)
	{
		if (moles[i].isHit(id))
		{
			hit = true;
			++score;
			menu.displayScore();
			time.level += 1000;	// 2 sec per killed mole
			++moleCounter;
			if (moleCounter >= level * 20)
			{
				moleCounter = 0;
				levelUp();
			}
			holes[id].innerHTML = '';
			moles[i].randomMole();		
		}
	}

	if (!hit)
	{
		--score;
		menu.displayScore();
		if (score < -10)	gameOver();

		time.level -= 10000;
	}


div = id = null;
}

function countDown (t) {
	if (!time.startFrame)	time.startFrame = t;
	if (!time.lastFrame)	time.lastFrame = t;
	if (!time.gameLastMS)	time.gameLastMS = t - time.startFrame;
	time.deltaFrame =  t - time.lastFrame;		// Math.round( * 1000) / 1000;
	time.gameTotalMS = t - time.startFrame;
	time.lastFrame = t;

	time.level -= time.deltaFrame;
	menu.displayTime();




	for (let i = moles.length-1; i >= 0; --i)
	{
		moles[i].animate(time.deltaFrame);
	}


	if (time.level <= 0)
	{
		gameOver();
		return;
	}
rafId = requestAnimationFrame(countDown);
}

function clearHoles () {

	for (let i = holes.length-1; i >= 0; --i)
	{
		holes[i].innerHTML = '';
	}
}



function gameOver () {

	cancelAnimationFrame(rafId);
	gameOn = false;
	menu.displayTime();
	menu.displayScore();

	let str = '';

	if (score > 0)
	{
		str = `Score: ${score}    Level: ${level}

	Well done :)`;
	}
	else if (score < 0)
	{
		str = `How did you do that? NEGATIVE score?!?

	You must be something special :)

	Score: ${score}  Level: ${level} Time left: ${Math.floor(time.level/1000)}`;
	}
	else
	{
		str = `Zero score! Really?
	That took some effort ;)`;
	}
	alert(str);

	resetGame();
}

function levelUp () {

	cancelAnimationFrame(rafId);
	gameOn = false;

	str = `Score: ${score}    Level: ${level}
Well done :)

Ok for next Level!
Cancel to stay on same ground.`;
	if (confirm(str))
	{
		++level;
		moles.push(new Mole());
	}
	

	nextLevel(level);
}


function nextLevel (l=1) {
	clearHoles();
	gameOn = true;
	resetTime(time.level + l * 10000);
	rafId = requestAnimationFrame(countDown);
}

function resetGame () {

	cancelAnimationFrame(rafId);
	clearHoles();
	moles.length = 0;
	moles.push(new Mole());
	gameOn = false;
	moleCounter = 0;
	score = 0;
	menu.displayScore(score);
	level = 1;
	resetTime(level * 60000);
	menu.displayTime();
	menu.setStartButton('start');
}


function Mole () {

let pos;
let downTime;
let upTime;
let digUpTimeTotal = 1000;
let digUpTime = digUpTimeTotal;
let digDownTimeTotal = 1000;
let digDownTime = digDownTimeTotal;

let inHole = false;
let imgElem = new Image();

function init () {

	imgElem.src = imgPath;
	imgElem.setAttribute('draggable', 'false');
	imgElem.setAttribute('user-select', 'none');
	imgElem.ondragstart = function () {return false;};
	imgElem.style.opacity = 0;

	randomMole();
}

this.isHit = function (p) {

	return inHole && pos === p;
};

this.animate = animate;
function animate (t) {

	if (downTime > 0)
	{
		downTime -= t;
		return;
	}

	if (!inHole) 
	{
		inHole = true;
		imgElem.style.opacity = 0;
		holes[pos].appendChild(imgElem);
		return;
	}

	if (digUpTime > 0)
	{
		digUpTime -= t;
		imgElem.style.opacity = 1 - (digUpTime / Math.max(200, digUpTimeTotal - level * 100));
		return;
	}


	if (upTime > 0)
	{
		upTime -= t;
		return;
	}

	if (digDownTime > 0)
	{
		digDownTime -= t;
		imgElem.style.opacity = digDownTime / Math.max(200, digDownTimeTotal - level * 100);
		return;
	}


	inHole = false;
	holes[pos].innerHTML = '';
	--score;
	menu.displayScore();
	time.level -= 10000;
	randomMole();	
}


this.randomMole = randomMole;
function randomMole () {

	inHole = false;
	pos = Math.floor(Math.random() * holes.length);
	downTime = Math.max(200, (Math.random() * 1000 - level * 100));
	upTime = Math.max(200, (Math.random() * 3000 + 2000 - (level * 1000)));

	digUpTime = Math.max(200, digUpTimeTotal - level * 100);
	digDownTime = Math.max(200, digDownTimeTotal - level * 100);
	imgElem.style.opacity = 0;
}


init();
}


function Menu () {
	
	const menuElem = document.createElement('DIV');

	const scoreElem = document.createElement('DIV');
	const timeLeftElem = document.createElement('DIV');
	const startButton = document.createElement('DIV');
	const menu = this;


function init () {
	menuElem.style.userSelect = 'none';
	menuElem.style.overflow = 'hidden';
	menuElem.style.position = 'relative';
	menuElem.style.display = 'flex';
	menuElem.style.justifyContent = 'space-around';
	menuElem.style.padding = '0.5em 0';
//	menuElem.innerHTML = 'menu';

	let wrap = document.createElement('DIV');

	startButton.addEventListener('click', onStart);
	startButton.style.border = '1px solid red';
	startButton.style.borderRadius = '2%';
	startButton.style.backgroundColor = '#ff801c';
	startButton.style.textAlign = 'center';
	startButton.style.fontWeight = 'bold';
	startButton.innerHTML = 'Start';
	startButton.style.padding = '0.2em';
	wrap.appendChild(startButton);
	menuElem.appendChild(wrap);


	wrap = document.createElement('DIV');
//	wrap.style.flex = 1;
	wrap.innerHTML = 'Time:';
	wrap.appendChild(timeLeftElem);
	menuElem.appendChild(wrap);

	wrap = document.createElement('DIV');
//	wrap.style.flex = 1;
	wrap.innerHTML = 'Score:';
	wrap.appendChild(scoreElem);
	menuElem.appendChild(wrap);


	container.appendChild(menuElem);
}
function onStart () {

	if (gameOn)
	{
		gameOn = false;
		menu.setStartButton('start');
		cancelAnimationFrame(rafId);
		resetTime(time.level);
	}
	else
	{
		gameOn = true;
		menu.setStartButton('pause');
		//console.log(performance.now());
		rafId = requestAnimationFrame(countDown);
	}
}

this.setStartButton = function (s) {

	if (s.toUpperCase() === 'PAUSE')
	{
		startButton.innerHTML = 'Pause';
		startButton.style.backgroundColor = '#8eff00';
	}
	else
	{
		startButton.innerHTML = 'Start';
		startButton.style.backgroundColor = '#ff801c';	
	}
};

this.displayScore = function () {
	scoreElem.innerHTML = score;
};
this.displayTime = function () {
	timeLeftElem.innerHTML = Math.floor(time.level / 1000) ;
};
init();
}

init();
} // ------------------ end BaseGame ()-------------------------
