/*
7 7 2020	


29 5 2020	separate files

23 4 2020	zestoko zezanje sa git ssh keys
*/
function GameLib () {
'use strict';
	var f = this;
	var o = this;
	GameLib.prototype.o = this;
	GameLib.o = this;

f.qelem = function qelem (elem) {

	if (typeof elem === 'string' && elem !== '') {

			elem = document.querySelector(elem);
			if (!elem)
			{
				console.warning('Could not find elem: '+elem);
				elem = null;
			return false;
			}
	}
	if (elem && typeof elem === 'object' && elem.nodeType === 1) {return elem;}

	// break js = dom connection
	elem = null;
return false;
};
o.TestJS = TestJS;
function TestJS () {


this.equal = function (txt, a, b) {

	if (a !== b) console.error(txt, a, b);
};

}


testNodeChain();
function testNodeChain () {

let t = new TestJS();

console.clear();
let l = new NodeChain();

t.equal('count', l.count(), 0);
t.equal('peekHead', l.peekHead(), null);
t.equal('peekTail', l.peekTail(), null);



t.equal('push', l.push('a'), 0);
t.equal('peekIndex', l.peekIndex(0), 'a');
t.equal('toString', l.toString(), 'a');
t.equal('peekHead', l.peekHead(), 'a');
t.equal('peekTail', l.peekTail(), 'a');

t.equal('push', l.push('b'), 1);
t.equal('peekIndex', l.peekIndex(1), 'b');
t.equal('toString', l.toString(), 'a, b');
t.equal('peekHead', l.peekHead(), 'a');
t.equal('peekTail', l.peekTail(), 'b');


t.equal('push', l.push('c'), 2);
t.equal('toString', l.toString(), 'a, b, c');
t.equal('peekIndex', l.peekIndex(2), 'c');
t.equal('count', l.count(), 3);
t.equal('peekHead', l.peekHead(), 'a');
t.equal('peekTail', l.peekTail(), 'c');


t.equal('indexOf', l.indexOf('a'), 0);
t.equal('indexOf', l.indexOf('b'), 1);
t.equal('indexOf', l.indexOf('c'), 2);
t.equal('indexOf', l.indexOf('d'), -1);
t.equal('indexOf', l.indexOf('a', 1), -1);


t.equal('pushHead', l.pushHead('d'), 0);
t.equal('toString', l.toString(), 'd, a, b, c');
t.equal('peekIndex', l.peekIndex(3), 'c');
t.equal('count', l.count(), 4);

t.equal('index0', l.peekIndex(0), 'd');
t.equal('peekIndex', l.peekIndex(1), 'a');
t.equal('peekIndex', l.peekIndex(2), 'b');
t.equal('peekIndex', l.peekIndex(3), 'c');

t.equal('count', l.count(), 4);
t.equal('del', l.delete('d'), 0);
//t.equal('', l.toString());
t.equal('toString', l.toString(), 'a, b, c');

//t.equal('', l.push('a'));
//t.equal('', l.toString())
t.equal('push', l.push('a'), 3);
//t.equal('', l.toString())
t.equal('delete', l.delete('b'), 1);
//t.equal('', l.toString());
t.equal('toString', l.toString(), 'a, c, a');
t.equal('count', l.count(), 3);

t.equal('push', l.push('d'), 3);
t.equal('peekIndex', l.peekIndex(2), 'a');
t.equal('delete', l.delete('d'), 3);
t.equal('toString', l.toString(), 'a, c, a');

t.equal('push', l.push('e'), 3);
t.equal('push', l.push('f'), 4);
t.equal('push', l.push('g'), 5);
t.equal('push', l.push('h'), 6);
t.equal('popIndex', l.popIndex(6), 'h');
t.equal('popIndex', l.popIndex(0), 'a');
t.equal('popIndex', l.popIndex(1), 'a');
t.equal('toString', l.toString(), 'c, e, f, g');
t.equal('peekHead', l.peekHead(), 'c');
t.equal('peekTail', l.peekTail(), 'g');


//console.log(l.toString());
l.setPointer();
t.equal('next', l.next(), 'c');
t.equal('next', l.next(), 'e');
t.equal('next', l.next(), 'f');
t.equal('next', l.next(), 'g');
t.equal('next', l.next(), null);
l.setPointer();
t.equal('next', l.next(), 'c');

t.equal('popHead', l.popHead(), 'c');
t.equal('count1', l.count(), 3);
t.equal('popTail', l.popTail(), 'g');
t.equal('count2', l.count(), 2);
l.push('a');
l.push('b');
l.push('c');
t.equal('count3', l.count(), 5);
t.equal('toStr1', l.toString(), 'e, f, a, b, c');
t.equal('peekHead', l.peekHead(), 'e');
t.equal('peekTail', l.peekTail(), 'c');


}

//performanceNodeChain();
function performanceNodeChain () {

const len = 1000000; 
let str = '';
let arr = [];

for (let mainLoop = 0; mainLoop < 10; ++ mainLoop) {
console.log('loop: ', mainLoop);

console.time('arr push');
for (let i = 0; i < len; ++i)
{
	arr.push('a');
}
console.timeEnd('arr push');		// 19.88

console.time('NC push');
let nc = new NodeChain();
for (let i = 0; i < len; ++i)
{
	nc.push('a');
}
console.timeEnd('NC push');			// 114.92


// --------------------------------------------------

str = '';
console.time('arr read');
for (let i = 0; i < len; ++i)
{
	str += arr[i];
}
console.timeEnd('arr read');		// 175.75


str = '';
console.time('NC read');
nc.reset();
let s = nc.next();
while(s)
{
	str += s;
	s = nc.next();
}
//console.log(str.length);
console.timeEnd('NC read');
// --------------------------------------------------

str = '';
console.time('ARR slice');
for (let i = 0; i < 2000; ++i)
{
	str += arr.splice(len/2, 1)[0];
	//arr.splice(0, 1);
}
console.timeEnd('ARR slice');

str = '';
console.time('NC slice');
for (let i = 0; i < 2000; ++i)
{
	str += nc.popIndex(len/2);
	//str += nc.popIndex(0);
}
console.timeEnd('NC slice');
console.log(str.length);


// --------------------------------------------------







console.log(arr.length);
console.log(nc.count());
arr.length = 0;
nc.clear();

}// end for
}// ------------------ end performance NodeCain ---------------



/*	10 7 2020
NodeChain
reset, next, delete, deleteIndex, push pop head tail
tests,


former LinkedList


to do:
pointer to nextNode() previousNode(), popNode(), peekNode(), pushNode()
slice (index, howManu)
pointerNext()


usage:
let nc = new NodeChain();

nc.push('a');
val = nc.pop();

nc.reset()
val = nc.next();

val = nc.peekIndex(2);
oldVal = nc.setIndex(2, newVal);
index = nc.indexOf(val);

nc.delete(val);
nc.deleteIndex(3);

val = nc.peekHead();
val = nc.peekTail();

val = nc.popHead();
val = nc.popTail();

*/
o.NodeChain = NodeChain;
function NodeChain () {

	let head = null;
	let tail = null;
	let length = 0;
	let pointer = head;
	let that = this;

// 10 7 2020	extra spor
function Node (v) {
	this.v = v;
//	this.prev = null;	// one day
	this.next = null;
}


/**	10 7 2020
requires .reset() before first .next()	call


return current node.v	and jump to next node
*/
this.next = function () {
	if (pointer === null)	return null;

	let ret = pointer.v;
	pointer = pointer.next;

	return ret;
};

/**	10 7 2020

set pointer to head
*/
this.setPointer = function (p = 0) {
	if (p == 0)	pointer = head;
	else pointer = nodeIndex(p);	// default null or head???

};

// should be internal
function deleteNode (node, prev) {



}

/**
 * return old value, 
 * @param {any} v
 */
this.replaceHead = function (v) {

	if(head === null)
	{
		pushHead(v);
		return null;
	}
	let old = head.v;
	head.v = v;
	return old;
};

// return old value
this.replaceTail = function (v) {

	if (tail === null)
	{
		pushHead(v);
		return null;
	}

	let old = head.v;
	head.v = v;

return old;
};


// 10 7 2020
this.peekHead = function () {
	return head ? head.v : null;
};
// 10 7 2020
this.peekTail = function () {
	return tail ? tail.v : null;
};


// popHead, popTail

this.pop = popTail;
this.popTail = popTail;
function popTail () {

	if (!tail)	return null;
	return that.popIndex(length-1);
}


this.popHead = function () {

	if (!head)	return null;
	let ret = head.v;

	--length;

	if (head.next)
	{
		head = head.next;
	}


	return ret;
};



// -------------------------------- PUSH ADD NODE ------------------------------
/**	10 7 2020

return index of pushed item
 */
this.push = pushTail;
this.pushTail = pushTail;
function pushTail (v) {

//	let n = new Node(v);	// 10 times slover !!!
	let n = {'v' : v, 'next' : null};
	if (head === null)
	{
		head = n;
		tail = n;
	}
	else
	{
		// tail is head or last node
		tail.next = n;
		tail = n;
	}
	return length++;
}
/**	10 7 2020

return 0 as first index
*/
this.pushHead = pushHead;
function pushHead (v) {

//	let n = new Node(v);		// 10 times slower
	let n = {'v' : v, 'next' : null};

	if (head === null)
	{
		head = n;
		tail = n;
	}
	else
	{
		n.next = head;
		head = n;
	}
	++length;

	return 0;
}
// ----------------------------- end PUSH ADD NODE -----------------------------








/**	15 7 2020


return node
 */
function nodeIndex (i, start = 0) {

	if (i < 0 || i >= length || start < 0 || start >= length)	return null;	// check index bounds

	let index = 0;
	let n = head;

	while (n)
	{
		if (index === i)	return n;
		n = n.next;
		++index;
	}
	return null;
}

/**	15 7 2020


return node 
*/
function nodeValue (v, start = 0) {

	if (start < 0 || start >= length)	return null;

	let n = head;
	let index = 0;

	while (n) {
		if (index >= start && n.v === v)	return n;
		n = n.next;
		++index;
	}
	return null;
}

/*	15 7 2020

return index if found, -1 if not
*/
this.indexOf = function (v, start = 0) {

	if (start < 0 || start >= length)	return -1;	// check index bounds

	let index = 0;
	let n = head;


	while (n)
	{
		if (index >= start && n.v === v)	return index;
		n = n.next;
		++index;
	}
	return -1;
};

/**	10 7 2020

return value if index exist, null if not 
*/
this.peekIndex = function (i) {

	let n = nodeIndex(i);
	return n === null ? null : n.v;
};


// del last found

/** 10 7 2020
deleteIndex

return deleted value
*/
this.popIndex = function (i) {

	if (i < 0 || i >= length)	return null;	// index out of bounds

	let n = head;
	let prev = head;
	let index = 0;
	let ret;

	while (index !== i) {
		prev = n;
		n = n.next;
		++index;
	}

	--length;
	ret = n ? n.v : null;

	if (index === 0)				// pop head
	{
		if (head.next === null)		// only head
		{
			head = null;
			tail = null;
			if (length !== 0)	console.error('NodeChain.popIndex: head index error ');
		}
		else	head = head.next;	// remove head

		return ret;
	}

	// found node
	prev.next = n.next;				// cut & join

	if (n.next === null)	tail = prev;
	n = null;

	return ret;
};


/**
delete first value

return index on delete, -1 on not found
*/
this.delete = function (v) {

	let n = head;
	let prev = head;
	let index = 0;
	while (n && n.v !== v) {
		prev = n;
		n = n.next;
		++index;
	}

	if (head && head.v === v)
	{
		--length;

		if (head.next === null)		// only head
		{
			head = null;
			tail = null;
			if (length !== 0)	console.error('NodeChain.delete: head index error!');
		}
		else	head = head.next;	// remove head

		return index;
	}


	if (n && n.v === v)				// found node
	 {
		--length;

		prev.next = n.next;			// cut

		if (n.next === null)	tail = prev;
		n = null;

		return index;
	}
	else	return -1;
};



// ???
this.deleteAll = function (v) {

	let n = head;
	let prev = head;
	let index = 0;
	while (n && n.v !== v) {
		prev = n;
		n = n.next;
		++index;
	}

	if (head && head.v === v)
	{
		--length;

		if (head.next === null)		// only head
		{
			head = null;
			tail = null;
			if (length !== 0)	console.error('NodeChain.deleteAll: head index error!');
		}
		else	head = head.next;	// remove head

		return index;
	}


	if (n && n.v === v)				// found node
	 {
		--length;

		prev.next = n.next;			// cut

		if (n.next === null)	tail = prev;
		n = null;

		return index;
	}
	else	return -1;
};



/**	10 7 2020
force - every node = null

delete chain
*/
this.clear = function (force = false) {

	if (head === null) {
		head = tail = null;
		length = 0;
		return;
	}

	if (force)
	{
		let node = head;
		while (node.next)
		{
			let tmp = node.next;
			node = null;	// del node
			node = tmp;
		}
		node = null;
	}

	head = tail = null;
	length = 0;
};

// 10 7 2020
this.toString = function () {

	let str = '';
	let n = head;
	while (n)
	{
		str += n.v;
		str += n !== tail ? ', ' : '';
		n = n.next;
	}

	return str;
};

// 10 7 2020
this.count = function () {
	return length;
};


} // ----------------------- end LinkedList -------------------------

function rndCol() {
	return '#' + ('00000' + Math.floor(Math.random() * 0x1000000).toString(16)).slice(-6);
}


// high 1-8
f.rndRBG = function (high)
{
	high = high >= 1 && high <= 8 ? high : 8;

    return "rgb(" + Math.floor(Math.random() * 32 * high) + ',' +
	Math.floor(Math.random() * 32 * high) + ',' +
	Math.floor(Math.random() * 32 * high) + ')';	
};


/* 1,8 light == 0-255; 8,1 == 1,7

low settings 1-8
high setting 

*/
f.rndRGBlh = function (low, high)
{
	low = low >= 1 && low <= 8 ? low-1 : 0;
	high = high >= 1 && high <= 8 ? high : 8;
	high = high - low;

    return "rgb(" + Math.floor((32 * low) + Math.random() * 32 * high) + ',' +
	Math.floor((32 * low) + Math.random() * 32 * high) + ',' +
	Math.floor((32 * low) + Math.random() * 32 * high) + ')';	
};


f.hsl2rgb = function (h,s,l)
{
  let a=s*Math.min(l,1-l);
  let f= (n,k=(n+h/30)%12) => l - a*Math.max(Math.min(k-3,9-k,1),-1);                 
  return [f(0),f(8),f(4)];
};   


f.getFirstEmptyElem  = function getFirstEmptyElem (q) {

	let elems = document.querySelectorAll(q);
	if (!elems || !elems.length)
	{
		console.warning('Could not find elems:' + q);
		elems = null;
		return false;
	}

	for (let i = 0, len = elems.length; i < len; ++i) {

		if (elems[i].innerHTML === '') return elems[i];
	}

	return false;
};



o.Point = function (x, y) {
	
	this.x = parseFloat(x, 10) || 0;
	
	y = parseFloat(y, 10);
	y = y || 0;	
	this.y = y;
};




o.Keyboard = function (elem, getUDLR, setDirection) {

	if (!elem || typeof elem !== 'object')
	{
		console.error('No elem im Keyboard');
		return;
	}

	const direction = {u:0, d:0, l:0, r:0};
	let max = 1;

	elem.addEventListener('keyup', keyUp);
	elem.addEventListener('keydown', keyDown);

function keyDown (e) {

	let dir = getUDLR(e.key || e.which || e.keyCode);


	if (direction[dir] === undefined)
	{
		e.preventDefault();	
		return;
	}

	direction[dir] = max++;

	setDirection(direction);

	e.preventDefault();	
}

function keyUp (e) {
	let dir = getUDLR(e.key || e.which || e.keyCode);

	if (direction[dir] === undefined)
	{
		e.preventDefault();	
		return;
	}

	let value = direction[dir];
	direction[dir] = 0;
	--max;

	for (let k in direction)
	{
		if (direction[k] >= value)	--direction[k];
	}

	setDirection(direction);

	e.preventDefault();	
}


};// --------------------------- end Keyboard() --------------------------------


/*

*/

/**
ok	ok =true
okc	ok = true, cancel = null
ync	yes = true, no = false,  cancel = null

gl.msgBox(text, 'okc', function (ok){ if (ok)});

*/
f.msgBox = function (str='', type='ok', onUserAction=null) {

	var full, box, text, buttons;
	full = document.createElement('DIV');
	full.style.userSelect = 'none';
	full.style.display = 'flex';
	full.style.zIndex =  2147483647;
	full.style.position = 'fixed';
	full.style.top = 0;
	full.style.bottom = 0;
	full.style.left = 0;
	full.style.right = 0;
//	full.style.height = '100vh';
//	full.style.width = '100vw';
	full.style.backgroundColor = '#00000040';


	box = document.createElement('DIV');
	box.style.margin = 'auto';
	box.style.display = 'flex';
	box.style.flexFlow = 'column nowrap';
	box.style.minWidth = '30%';
	box.style.maxWidth = '60%';
	box.style.minHeight = '25%';	
	box.style.maxHeight = '75%';
	box.style.borderRadius = '1rem';
	box.style.border = '2px solid darkgray';
	box.style.backgroundColor = '#FFFFFFaa';


	full.appendChild(box);

	text = document.createElement('DIV');
	text.style.userSelect = 'none';
	text.style.flexGrow = 1;
	text.style.padding = '1rem';
	text.style.whiteSpace = 'pre-wrap';
	text.style.overflow = 'auto';
	text.innerHTML = str;
//	text.style.border = '2px solid green'
	box.appendChild(text);



	buttons = document.createElement('DIV');
	buttons.style.display = 'flex';
	buttons.style.justifyContent = 'space-around';
	buttons.style.fontWeight = 'bold';
	buttons.style.padding = '1rem 0';
//	buttons.style.alignSelf = 'flex-end';
	box.appendChild(buttons);	// for .focus()

	switch(type.toUpperCase())
	{
	case 'OK':
		okButton();
	break;
	
	case 'OKC':

		okButton();
		cancelButton();
	break;

	case 'YNC':
		yesButton();
		noButton();
		cancelButton();
	break;


	default:
		console.error('wrong type in msgBox('+type+', string, function)');
		okButton();		
	}



	if (document.fullscreenElement)
	{
		document.fullscreenElement.appendChild(full);

	}	else	document.body.appendChild(full);


function okButton () {
	createButton('OK', true);
}
function cancelButton () {
	createButton('CANCEL', null);
}
function yesButton () {
	createButton('YES', true);
}
function noButton () {
	createButton('NO', false);
}
function createButton (str, retVal) {
	
	var button = document.createElement('BUTTON');
	button.style.userSelect = 'none';
	button.innerHTML = str;
	button.style.padding = '0.5em';
	button.style.borderRadius = '10%';
	button.tabIndex = 0;
	button.onclick = function () {
		
		this.onclick = null;
		full.parentNode.removeChild(full);
		if (typeof onUserAction === 'function') {
			onUserAction(retVal);
		}
		button = null;
	};

	buttons.appendChild(button);
	button.focus();

//	button = null;
}


box = text = buttons = null;
};

/**
usage:

const fsButton = new gl.FullScreenButton(container);
menu.appendChild(fsButton.getElem());

 */
o.FullScreenButton = function (fsElem, smallScrTxt='FullScr', fullScrTxt='Exit fullScr') {

	if (!fsElem)	console.error('Missing full screen element!');

	const fullScreenButton = document.createElement('BUTTON');
	fullScreenButton.innerHTML = smallScrTxt;
	fullScreenButton.style.display = 'flex';
	fullScreenButton.style.justifyContent = 'center';
	fullScreenButton.style.alignItems = 'center';
	fullScreenButton.style.borderRadius = '0.3rem';
	fullScreenButton.style.fontSize = 'inherit';
	fullScreenButton.title = 'Full Screen';
	fullScreenButton.addEventListener('click', function () {
	
		if (document.fullscreenElement)
		{
			document.exitFullscreen();
//			fullScreenButton.innerHTML = smallScrTxt;
//			full = false;
		}
		else
		{
			var promise = fsElem.requestFullscreen();
//			fullScreenButton.innerHTML = fullScrTxt;
//			full = true;
		}
	});

	// catch button and ECS key
	document.addEventListener('fullscreenchange', function (e) {

		if (document.fullscreenElement)	
		{	// in full screen
			fullScreenButton.innerHTML = fullScrTxt;
			fullScreenButton.title = 'Exit full screen';
		}
		else
		{
			// in small screen
			fullScreenButton.innerHTML = smallScrTxt;
			fullScreenButton.title = 'Full Screen';
		}
	});


this.getElem = function () {return fullScreenButton;};
};


o.Sound = function (volume = 0.5) {

	var audio = {};
	var key;
	var on = true;

var type = {
	'mp3' : 'audio/mp3',
	'ogg' : 'audio/ogg',
};

this.i = function (k) {
	if (!audio[k])
	{
		audio[k] = { 
			elem : document.createElement('AUDIO'),
			playing : false,
		};
		audio[k].elem.style.display = 'none';
		//document.body.appendChild(audio[k].elem);
		audio[k].elem.setAttribute('preload', 'auto');
		audio[k].elem.volume = volume;
	}

	key = k;
	return this;
};

this.setOn = function (v=true) {
	on = !!v;
};
this.on = function () {on = true;};
this.off = function () {on = false;};

this.volume = function (v = null) {

	if (v=== null) return volume;
	else volume = v;
};

this.src = function (src) {
	if (!audio[key]) return;

	let source = document.createElement('SOURCE');
	source.setAttribute('src', src);
	source.setAttribute('type', type.mp3);

	audio[key].elem.appendChild(source);
};


this.play = play;
function play (ct = 0) {
if (!on || !audio[key] || !audio[key].elem.readyState)		return;

	if (ct >= 0) audio[key].elem.currentTime = ct;
//	audio[key].playing = true;
//	audio[key].elem.play();		// assync 

	let playPromise = audio[key].elem.play();

	if (playPromise !== undefined)
	{
		playPromise.then(_ => {
			audio[key].playing = true;
			//console.log(audio[key].elem);
		})
		.catch(error => {
			audio[key].playing = true;
		});
	}
}

this.pause = pause;
function pause () {
if (!audio[key] || !audio[key].playing)		return;

	audio[key].playing = false;
	audio[key].elem.pause();
}

this.stop = stop;
function stop () {
if (!audio[key] || !audio[key].playing)		return;

	audio[key].playing = false;
	audio[key].elem.currentTime = 0;
	audio[key].elem.pause();
}

};



}// ---------------------------- end GameLib -----------------------------------
