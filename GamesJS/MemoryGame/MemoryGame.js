/*
29 5 2020	adding timeout,  5 sec per card? 3 sec ok

to do:
hard
rnd
getItem(1-4, hard) {}


http://soundbible.com/2068-Woosh.html
http://soundbible.com/1821-IMac-Startup-Chime.html
http://soundbible.com/1744-Shooting-Star.html
http://soundbible.com/101-Golf-Club-Swing.html
*/
function MemoryGame (outerElem) {

	let that = this;
	if (!outerElem || typeof outerElem !== 'object')
	{
		console.log('No outer elem: '+ outerElem);	
		return false;
	}


const cardBack = [' ','▮','▯','🂠','✠','⚛','▦','▩'];
const cardFace = {
normal : [
 ['⚀','⚂','⚃','⚚','⯅','🕀','❤','♪','♫','🎜','🗚','☊','☛','☜','🗩','🗪','⛨','⛏','⚒','🕱','🌩','🌧','☁','☀','☂','☥','✝','☩','☪','⚊','⚏','⚌','⚟','✔','☘','☾','🗥','🗧','☸','✈','🏍','🕸','♂','♀','⚤','⛹','☢','☯','🏳','⛸','🕮']
,['⚁','⚄','⚅','⚘','⯀','🕁','❥','♩','♬','🎝','🗛','☋','☚','☞','🗨','🗫','⛰','☭','⚔','☠','🌪','🌨','🌤','❄','⛱','☤','☦','✠','✡','⚋','⚍','⚎','⚞','✘','✾','☽','🗤','🗦','⚛','⛴','⛟','🕷','⚣','⚢','⚥','🏋','☣','☮','⛿','⛷','🕯']
]
,smiley : [
 ['🤢','🤕','🤒','🥵','😎','😮','😳','😦','😤','🤑','😢','🙄','🥰','😘','🤭','🤔','🤐','😀','😄','🤣','🙂','😜','😋','😇','👦','👨','👴','👲','👷','👻','🤚','👆','👈','👍','👊','👋','👂','👄','👀','👾']
,['🤮','😷','🤧','🥶','🤓','😲','🥺','😧','😡','😵','😭','😬','😍','😗','🤫','🧐','😬','😃','😁','😂','🙃','🤪','😛','😈','👧','👩','👵','👳','👶','💀','✋','👇','👉','👎','👌','🖖','👃','💋','👅','👽']
]
,blackWhite : [
 ['♚','♛','♜','♝','♞','♟','☎','★','✂','▰','▮','▶','◀','⧫','✦','⯌','⯍','☗','⛊','★','⚑','🕇','✿','🏶','♻','⛇','♠','♦','♣','♥','☻','🏲']
,['♔','♕','♖','♗','♘','♙','☏','☆','✄','▱','▯','▷','◁','◊','✧','⯎','⯏','☖','⛉','☆','⚐','🕆','❀','🏵','♲','☃','♤','♢','♧','♡','☺','🏱']
]
,astro : [
 ['⍟','⊛','✲','✷','✹','✸','✴','❉','📡','🛰','💥','🌀','🌊','🌚','🌕','🌘','🌗','🌖','🌅','🌐','🌎','🌝','🌜', '☿','♁','♃','♄','♆','♇','☉','♈','♉','♊','♋','♌','♍','☼']
,['✪','❂','✱','⚝','✵', '✶','✺','❋','🔭','🚀','☄','🌌','🌈','🌑','🌙','🌒','🌓','🌔','🌄','🌏','🌍','🌞','🌛','⚳','⚴','⚵','⚶','♅','⚷','🌣','♎','♏','♐','♑','♒','♓','⛎']
] //🌋
,animal : [
 ['🐸','🐭','🐺','🐼','🐮','🐯','🐶','🐷','🐵','🦀','🦑','🐚','🦈','🐟','🐋','🦕','🦆','🦚','🦉','🦃','🦔','🐤','🐍','🐁','🐂','🐅','🐇','🐕','🐏','🐐','🐓','🐉','🐪','🐌','🐎','🦏','🦒','🐒','🐜','🐛','🐝','🐦',]
,['🐲','🐹','🐗','🐻','🐨','🐱','🐰','🐴','🐾','🦞','🐙','🐡','🐬','🐠','🐳','🦖','🦢','🦜','🦅', '🕊','🦇','🐥','🦎','🐀','🐃','🐆','🐈','🐩','🐑','🐖','🐔','🐊','🐫','🐢','🐄','🦛','🐘','🦙','🐝','🐞','🦗','🐧']
]
,plant : [
 ['🍀','🌳','🌵','🌻','🌷','🍁','🌸','💐','🌰','🍎','🍐','🍇','🍈','🍋','🍒','🍌','🍅','🌾','🥥','🥕','🥭','🥦','🍠']
,[ '☘','🌿','🌲','🌴','🌼','🌹','🍂','🌺','🌱', '🍏','🍑','🍆','🍉','🍊','🍓','🍍','🍄','🌽','🥑','🥔','🥝','🥜','🥬']
] //🌶
,food : [
 ['🍿', '🍤','🥨','🥐','🍫','🍨','🍮','🥮','🎂','🥗','🥘','🍲','🥣','🍞','🍜','🥫','🍣','🌭','🥙','🍕','🍡','🍟','🥩','🍗','🍦','🍭','🍪','🍺','🥃','🍸','🍹','☕','🥛']
,['🥟','🧀','🥖','🥯','🍯','🥞','🥧','🧁','🍰','🍥','🍳','🍵','🍛','🍚','🍝','🍱','🌯','🍔','🌮','🥠','🍢','🍙','🥓','🍖','🍧','🍬','🍩','🍻','🥂','🍷','🥤','🍶','🍾']
]
,thing : [
 ['🔦','🗻','🗼','🔪','📢','👒','🏁','🎲','🚇','🚢','🚹','🚽','🚗','🚓','🚑','🚃','🚄','🔍','🎉','🔌','🏈','⚽','🏐','🎈','💡', '💙','💛','💖','💲','📌','💦','📐','📘','💿','📕','🔒','🔧','🔉','💍','🏅','🎻','🎷','🎨','🎧','🎭','🎮' ,'🏓','🏰','🏫','🏠','🏑','👗','👟','👠','👢']
,['🔩', '🗿','🗽','🔫','📣','👓','🎌','🎳','🚉','🚤','🚺','🛀','🚙','🚕','🚒','🚌','🚅','🔎','🎊', '🔋','🏉','⚾','🏀','🎐','💣','💚','💜','💔','💰','📍','💧', '📏','📙','📀','📗','🔓','🔨','🔊','💎','🏆','🎸','🎺','🎩','🎤','🎪','🎥','🏸','🏯','🏥','🏡','🏒','👖','👞','👡','👣']
]
,number : [
 ['❶','❷','❸','❹','❺','❻','❼','❽','❾','❿','1','2','3','4','5','6','7','8','9','0']
,['➊','➋','➌','➍','➎','➏','➐','➑','➒','➓','➀','➁','➂','➃','➄','➅','➆','➇','➈','➉']
]
,clock : [
 ['🕛','🕐','🕑','🕒','🕓','🕔','🕕','🕖','🕗','🕘','🕙','🕚']
,['🕧','🕜','🕝','🕞','🕟','🕠','🕡','🕢','🕣','🕤','🕥','🕦']
]
,half :[
 ['☰','☱','☲','☳','◰','◶','◱','◷','◐','◗','◒','⯊','◔','⬒','⬘','⬔','⬗','▴','▸','▵','▹']
,['☴','☵','☶','☷','◲','◴','◳','◵','◑','◖','◓','⯋','◕','⬓','⬙','⬕','⬖','▾','◂','▿','◃']
]

,arrow1 : [
 ['⮘','⮙','⮞','⮟','✎','✏','❧','➴','➵','➶','⮈','⮉','⮎','⮏','⭯','⥀','↷','⤸','⤾','↜','⬿','⮨','⮬','⮫','⮯','⮰','⮴','⮳','⮷','⮠','⮤','⮣','⮧','↩','↫','⮐','⮒','⭜','⭚','⭎','⭞','⭍']
,['⮚','⮛','⮜','⮝','✐','❦', '☙','➷','➸','➹','⮊','⮋','⮌','⮍','⭮','⥁','↶','⤹','⤿','↝','⤳','⮪','⮭','⮩','⮮','⮲','⮵','⮱','⮶','⮢','⮥','⮡','⮦','↪','↬','⮑','⮓','⭝','⭛','⭏','⭟','↯']
]
,arrow2 : [
 ['↼','↾','⇁','⇃','↚','←', '↑','→','↓','↖', '↗','↘','↙','⭥','⤡','⮀','⮁','⮅','⮄','⭾','↢','⇷','↤','↥','⇹','⬹','⭺','⇺','⬾','⭻','⬺','⯬','↞','↟','⯭','⬴','⬶','⬻', '⬵','⬼','⬽','⥷','⥺']
,['↽','↿','⇀','⇂','↛','⭰','⭱','⭲','⭳','⭶','⭷','⭸','⭹','↨','⤢','⮂','⮃','⮇','⮆','⭿','↣','⇸','↦','↧','⇼','⤔','⭼','⇻','⥇','⭽','⤕','⯮','↠','↡','⯯','⤀','⤅','⤖','⤁','⤗','⤘','⭃','⭄']
]




};	// --------------- end cardFace {} -----------------------------------------
const cardFaceKeys = Object.keys(cardFace);

const audio = new gl.Sound(0.01);
const container = document.createElement('DIV');
const boardElem = document.createElement('DIV');
const deck = [];
const settings = {
	timePerCard : 3,
	cardBackIndex : 3,
	requiredCards : 2,	// find number
	deckRange : 1,
	deckLength : 4,	// precals len by range2len(1) 
	cardFaceIndex : 4,
	faceUpDurationMs : 500,
};
let boardW = 320, boardH = 320, cardW = 0, cardH = 0,
	chosenCardId = [],
	setTimeoutId,
	cardCounter = 0,	// for levelOver
	score = 0,
	totalMoves = 0,
	timeOutCounter = settings.deckLength * settings.timePerCard * 1000,
	timeOutId = null,
	previousTime,
	inGame = false;

const menu = new Menu();	// must be here, order is important






function init () {

	window.addEventListener('resize', onResize);
/*
	audio.faceUp = new Audio('MemoryGame/snd/Woosh-Mark_DiAngelo-4778593.mp3');
	audio.levelOver = new Audio('MemoryGame/snd/shooting_star-Mike_Koenig-1132888100.mp3');
	audio.cardsMatch = new Audio('MemoryGame/snd/Golf Club Swing-SoundBible.com-1724786007.mp3');
*/
	audio.i('faceUp').src('MemoryGame/snd/Woosh-Mark_DiAngelo-4778593.mp3');
	audio.i('levelOver').src('MemoryGame/snd/shooting_star-Mike_Koenig-1132888100.mp3');
	audio.i('cardsMatch').src('MemoryGame/snd/Golf Club Swing-SoundBible.com-1724786007.mp3');

//	for (let a in audio)	a.volume = 1;


//debugger;
	container.style.userSelect = 'none';
	container.style.overflow = 'hidden';		// obavezno!
	container.style.position = 'relative';
	container.style.boxSizing = 'border-box';
	container.style.minWidth = boardW +'px';
	container.style.minHeight = boardH + 'px';
	container.style.width = '100%';
	container.style.height = '100%';
	container.style.display = 'flex';
	container.style.flexFlow = 'column nowrap';
	container.style.backgroundColor = '#FFFFFF';
	container.style.borderRadius = '1%';

	container.appendChild(menu.getElem());

	boardElem.style.userSelect = 'none';
	boardElem.style.overflow = 'hidden';		// obavezno!
	boardElem.style.flexGrow = 1;
	boardElem.style.boxSizing = 'border-box';	// you want only content size
	boardElem.position = 'relative';
	boardElem.style.userSelect = 'none';
	boardElem.style.display = 'flex';
	boardElem.style.flexFlow = 'row wrap';
	boardElem.style.justifyContent = 'space-around';	//'space-between' 'space-around';
	boardElem.style.alignItems = 'center';
	boardElem.style.alignContent = 'space-around';
	boardElem.style.backgroundColor = '#FFFFFF';		//'#fefed3';
//	boardElem.style.border = '1px solid green'
	boardElem.addEventListener('click', boardOnClick);
	container.appendChild(boardElem);

	outerElem.innerHTML = '';
	outerElem.appendChild(container);


	setBoardSizes();	// first
	menu.resize();


//debugger;
	refreshBoard();


}// ---------------------- end init () -----------------------


function onResize () {

	setBoardSizes();
	menu.resize();	// order is important!


	setCardSizes();

	for (let i = deck.length-1; i >= 0; --i)
	{
		deck[i].resize();
	}

}


function range2len (range) {

	let x = Math.floor(range/4)*2 + 2;
	let y = x - 1 + range % 4;
	// swap x > y
return x * y;
}

function setBoardSizes () {

	boardW = parseInt(boardElem.clientWidth, 10) || 320;
	boardH = parseInt(boardElem.clientHeight, 10) || 320;	// default size
}
function setCardSizes () {

	let space = 2;	// 1 ok. 4 max za 132 karte

//	let y = Math.floor(Math.sqrt(settings.deckLength));
//	let x = Math.floor(settings.deckLength / y);

	let x = Math.floor(settings.deckRange/4)*2 + 2;
	let y = Math.floor(x - 1 + settings.deckRange % 4);

	if (y > x) {
		let t = x;
		x = y;
		y = t;
	}
	settings.deckLength = x * y;

	cardW = Math.floor(boardW / x) - space;
	while (boardW <= cardW * x)	--cardW;

	cardH = Math.floor(boardH / y) - space;
	while (boardH <= cardH * y)	--cardH;

//console.log(boardW, boardH, cardW, cardH, settings.deckLength, x, y);
//console.log(boardW > (x * cardW), boardH > (cardH * y));
}

function boardOnClick (e) {
	e = e || window.event;
	let elem = e.target ? e.target : e.srcElement;			// elem you clicked on
	while (elem.nodeType != 1)	elem = elem.parentNode;		// if clicked on text node

	let cardId = e.target.getAttribute('data-id');			// null if no elem
	cardId = parseInt(cardId, 10);

//	1. return if no card, clicked in middle or dead card
	if (!inGame || cardId === null || isNaN(cardId) ||
		!deck[cardId] || !deck[cardId].inPlay())		return;
	

	menu.setTotal(++totalMoves);

//	2. user faster than timeout, already full arr
	if (chosenCardId.length >= settings.requiredCards)
	{
		clearTimeout(setTimeoutId);
		checkMatch();										// instant check
	}


//	3. clear same card clicked
	for (let i = chosenCardId.length-1; i >= 0; --i)
	{
		if (chosenCardId[i] === cardId)					// same card
		{
			chosenCardId.splice(i, 1);
			deck[cardId].faceDown();
			return;
		}
	}

//	4. clicked card
	let card = deck[cardId];
	card.faceUp();
	chosenCardId.push(cardId);


//	5. real check
	let len = chosenCardId.length;
	if (len < settings.requiredCards)
	{
		//semi check
		return;
	}
	else if (len >= settings.requiredCards)					// poslednja karta
	{
		//console.log('check delayed');
		setTimeoutId = setTimeout(checkMatch, settings.faceUpDurationMs);
	}
} // ---------------------- end boardOnClick () -------------------------


function checkMatch () {

	let same = 0, len = chosenCardId.length;
	if (len === 0)	return 0;			// must not pass len 0 or 1

	for (let i = 0; i < len; ++i)
	{
		let name1 = deck[chosenCardId[i]].getName();
		deck[chosenCardId[i]].faceDown();

		for (let j = i + 1; j < len; ++j)
		{
			if (name1 == deck[chosenCardId[j]].getName())
			{
				audio.i('cardsMatch').play();

				++same;
				deck[chosenCardId[i]].dead();
				deck[chosenCardId[j]].dead();			
			}
		}
	}

	chosenCardId.length = 0;

	score += same;
	menu.setScore(score);
	cardCounter += same;
	if (cardCounter >= settings.deckLength/2)
	{
		levelOver();
	}
}



/*
return []
x2,3,4
*/ 
function createFaceSymbols () {
//debugger;
	let faces = [];

	let totalCards = settings.deckLength / settings.requiredCards ;
	let firstIndex = settings.cardFaceIndex;
	let down = true;
	let used = {};
//	if (totalCards >= 60 && firstIndex == 2) debugger;
	while (faces.length < totalCards) {

//		let key = cardFaceKeys[Math.floor(Math.random() * cardFaceKeys.length)] ;
		


		let secondIndex = Math.round(Math.random());	// 0-1

		if (used[firstIndex] && used[firstIndex][secondIndex])
		{

			secondIndex = secondIndex ? 0 : 1;

			if (used[firstIndex][secondIndex])	continue;
		}

		if (!used[firstIndex]) used[firstIndex] = {};
		used[firstIndex][secondIndex] = true;


		let key = cardFaceKeys[firstIndex];
		let growLen = faces.length + cardFace[key][secondIndex].length;
		if (growLen <= totalCards)
		{
			faces = faces.concat(cardFace[key][secondIndex]);
			//continue;
		}
		else
		{
			let end = totalCards - faces.length;	// needed index
			let start = cardFace[key][secondIndex].length - end;
			start = Math.round(Math.random() * start);
			let arr = cardFace[key][secondIndex].slice(start, start + end);
			faces = faces.concat( arr);
		}


		down ? --firstIndex : ++firstIndex;
		if (firstIndex < 0)
		{
			down = false;
			firstIndex = settings.cardFaceIndex + 1;
		}	
	}
//console.log(faces);

	// multiply cards
	let l = faces.length * (settings.requiredCards - 1);
	while (l--)	faces.push(faces[l]);	// for values only

	faces.sort(()=> 0.5 - Math.random());
//console.log(faces);

	return faces;
}

function getRndFace () {





}

function createFaceSymbolsOld () {
//debugger;
	let s = '';

	let len = settings.deckLength / 2;

	let key = 'normal';
	let ind = 0;

	while (s.length < len) {

		let growLen = s.length + cardFace[key][ind].length;
		if (growLen <= len) {
		
			s += cardFace[key][ind];
			continue;
		} else {

			s += cardFace[key][ind].slice(0, len - s.length);
		}	
	}

	return s.split('');
}

  
function mergeArraysFilter(...arrays) {

	let jointArray = [];

	arrays.forEach(array => {
		jointArray = [...jointArray, ...array];
	});

	return jointArray.filter((item,index) => jointArray.indexOf(item) === index);
}

function createDeck () {
	deck.length = 0;
	boardElem.innerHTML = '';

	let faces = createFaceSymbols();

	let len = faces.length;
	let frag = document.createDocumentFragment();

	for (let i = 0; i < settings.deckLength; ++i)
	{
		let face = faces[i];		// Math.floor(i/2)		i % len	

		let card = new Card(i, face, cardBack[settings.cardBackIndex]);

		frag.appendChild(card.getElem());
		deck.push(card);
	}

	boardElem.appendChild(frag);
}

function refreshBoard () {

	boardElem.innerHTML = '';
//	setBoardSizes();	// moze, ne mora 
	setCardSizes();

	createDeck();

}
function levelOver () {

//	audio.i('levelOver').play();

gl.msgBox(`Level up!
It took you: ${totalMoves} tries
Score: ${score}

OK for next level, CANCEL to quit`, 'okc', function (ok){

	if (ok)	levelUp();
	else 	resetGame();
});
}

function gameOver () {

	pauseGame();

gl.msgBox(`Game Over
It took you: ${totalMoves} tries
Score: ${score}
`, 'ok', function () {

//	menu.setDeckRange(++settings.deckRange);
	resetGame();
});

}

function resetGame () {

	cardCounter = 0;
	score = 0;
	menu.setScore(score);
	totalMoves = 0;
	menu.setTotal(totalMoves);
	timeOutCounter = settings.deckLength * settings.timePerCard * 1000;
	menu.setTime(timeOutCounter);

	pauseGame();

	refreshBoard();
}
function levelUp () {

	menu.setDeckRange(++settings.deckRange);

	cardCounter = 0;
	totalMoves = 0;
	menu.setTotal(totalMoves);
	timeOutCounter += settings.deckLength * settings.timePerCard * 1000;
	menu.setTime(timeOutCounter);

	refreshBoard();
}

function startPauseGame () {

	if (inGame)	pauseGame();
	else		startGame();
}

function startGame () {

	inGame = true;
	previousTime = performance.now();
	timeOutId = setInterval(countDown, 300);	
	menu.setStartButton('PAUSE');
}
function pauseGame () {

	inGame = false;
	clearInterval(timeOutId);
	menu.setStartButton('START');
}


function countDown () {

	now = performance.now();
	if (now - previousTime >= 1000)
	{
//		console.log(now - previousTime)		~1200ms
		timeOutCounter -= now - previousTime;
		menu.setTime(timeOutCounter);

		previousTime = now;
	}

	if (timeOutCounter < 0)				// <= 0 display bug
	{
		menu.setTime(timeOutCounter);	// repaint display bug
		gameOver();
	}
}



function Card (i, f = '', b = '🂠') {

	const elem = document.createElement('DIV');
	const id = i;
	let back = b, face = f,
	faceUp = true,
	inPlay = true,
	fontRatio = 0.85,
	faceUpBgColor = '#FFFFFF',
	faceDownBgColor = '#DDDDDD';

function init () {

	elem.style.overflow = 'hidden';		// obavezno!
	elem.innerHTML = back;
	elem.setAttribute('data-id', id);
	elem.style.position = 'relative';
	elem.style.boxSizing = 'border-box';
	elem.style.width = cardW + 'px';
	elem.style.height = cardH + 'px';
	elem.style.fontSize = Math.round(Math.min(cardH,cardW) * fontRatio) + 'px';
//	elem.style.fontSize = '100vh';
//	elem.style.lineHeight = Math.floor(cardW * fontRatio) + 'px';
	elem.style.lineHeight = cardH + 'px';
	elem.style.userSelect = 'none';
	elem.style.borderRadius = '2%';
	elem.style.border = '1px solid darkgray';
	elem.style.backgroundColor = faceDownBgColor;
	elem.style.display = 'flex';
	elem.style.justifyContent = 'center';
	elem.style.alignItems = 'center';
//	elem.style.alignContent = 'center';

}

this.faceUp = function () {
	if (!inPlay)	return;

	audio.i('faceUp').play();
	

	faceUp = true;
	elem.innerHTML = face;
	elem.style.backgroundColor = faceUpBgColor;
};

this.faceDown = function () {
	if (!inPlay)	return;

	faceUp = false;
	elem.innerHTML = back;
	elem.style.backgroundColor = faceDownBgColor;
};
this.dead = function () {
	if (!inPlay)	return;		// da ne umre 2x, al nekad treba da umire vise puta

	inPlay = false;
	faceUp = false;

	elem.innerHTML = '';	// dead symbol
	elem.style.backgroundColor = 'transparent';
	//elem.style.border = '';
};


this.resize = function () {

	elem.style.width = cardW;
	elem.style.height = cardH;
	elem.style.fontSize = Math.round(Math.min(cardW, cardH) * fontRatio) + 'px';
};
this.inPlay = function () {
	return inPlay;
};

this.getId = function () {
	return id;
};

this.getElem = function () {
	return elem;
};
this.getName = function () {
return face;
};

init();
}// ---------------------- end Card () -----------------------


function Menu () {

const that = this;
const menuElem = document.createElement('DIV');
const scoreElem = document.createElement('DIV');
const totalElem = document.createElement('DIV');
const timeOutElem = document.createElement('DIV');
const startButton = document.createElement('BUTTON');

const deckRangeElem = document.createElement('INPUT');		// size 1-10
const displayDeckLengthElem = document.createElement('DIV');	// len size^2

const itemsRangeElem = document.createElement('INPUT');
const displayItemsElem = document.createElement('DIV');

const fontSize = 20;

function init () {
	let wrap, div, button, input, label, frag = document.createDocumentFragment();

	menuElem.style.userSelect = 'none';
	menuElem.style.overflow = 'hidden';
	menuElem.style.position = 'relative';
	menuElem.style.boxSizing = 'border-box';
	menuElem.style.display = 'flex';
	menuElem.style.justifyContent = 'space-between';
//	menuElem.style.fontSize = fontSize;
	menuElem.style.padding = '0.3em';
//	menuElem.style.borderRadius = '1%';

	that.resize();
	
	wrap = document.createElement('DIV');
	wrap.style.display = 'flex';
	wrap.style.flex = 1;	// 1 1 auto
	wrap.style.justifyContent = 'space-around';
//	wrap.style.alignSelf = 'flex-start';

	// SCORE
	div = document.createElement('DIV');
	div.style.textAlign = 'center';
	div.innerHTML = 'Score:';
	scoreElem.style.fontWeight = 'bold';
	scoreElem.style.textAlign = 'center';
	scoreElem.innerHTML = score;
	div.appendChild(scoreElem);
	wrap.appendChild(div);

	// TOTAL
	div = document.createElement('DIV');
	div.style.textAlign = 'center';
	div.innerHTML = 'Total:';
	totalElem.style.textAlign = 'center';
	totalElem.style.fontWeight = 'bold';
	totalElem.innerHTML = totalMoves;
	div.appendChild(totalElem);
	wrap.appendChild(div);

	// TIME
	div = document.createElement('DIV');
	div.style.textAlign = 'center';
	div.innerHTML = 'Time:';
	timeOutElem.style.textAlign = 'center';
	timeOutElem.style.fontWeight = 'bold';
	timeOutElem.innerHTML = Math.floor(timeOutCounter/1000);
	div.appendChild(timeOutElem);
	wrap.appendChild(div);
	frag.appendChild(wrap);

	// full screen, start pause, hard
	wrap = document.createElement('DIV');
	wrap.style.display = 'flex';
	wrap.style.flexFlow = 'row nowrap';	
	wrap.style.flex = '0 1 auto';
	wrap.style.justifyContent = 'space-between';
	wrap.style.fontSize = '90%';
//	wrap.style.border = '1px solid red';

	// FULL SCREEN
	button = document.createElement('BUTTON');
	button.innerHTML = 'FULL';
	button.onclick = function () {
		container.requestFullscreen();
	};
	wrap.appendChild(button);

	// START
	startButton.innerHTML = 'START';
	startButton.onclick = function () {
		startPauseGame();
	};
	wrap.appendChild(startButton);

	div = document.createElement('DIV'); 

	// HARD
	input = document.createElement('INPUT');
	input.setAttribute('type', 'checkbox');
	label = document.createElement('LABEL');
	label.style.display = 'block';
	label.innerHTML = 'Hard';
	label.prepend(input);
//	label.style.border = '1px solid red';
	div.appendChild(label);

	// RND
	label = document.createElement('LABEL');
	label.style.display = 'block';
//	label.style.border = '1px solid green';
	label.innerHTML = 'Rnd';
	input = document.createElement('INPUT');
	input.setAttribute('type', 'checkbox');
	label.prepend(input);
	div.appendChild(label);
	wrap.appendChild(div);

	frag.appendChild(wrap);


	// Deck Size
	wrap = document.createElement('DIV');
	wrap.style.marginLeft = '0.5em';
	wrap.style.display = 'flex';
	wrap.style.flexFlow = 'row wrap';	
	wrap.style.flex = 1;
	wrap.style.justifyContent = 'space-between';
	wrap.style.fontSize = '90%';
//	wrap.style.alignItems = 'center';
//	wrap.style.alignContent = 'center;'
	div = document.createElement('DIV');
	div.innerHTML = 'Deck size:';
	wrap.appendChild(div);

	displayDeckLengthElem.innerHTML = settings.deckLength;
	displayDeckLengthElem.style.fontWeight = 'bold';
	displayDeckLengthElem.style.textAlign = 'right';
	wrap.appendChild(displayDeckLengthElem);

	deckRangeElem.style.width = '100%';
	deckRangeElem.setAttribute('type', 'range');
	deckRangeElem.setAttribute('min', 1);
	deckRangeElem.setAttribute('max', 20);
	deckRangeElem.setAttribute('value', settings.deckRange);
	deckRangeElem.addEventListener('input', deckRangeChange);	// change
	wrap.appendChild(deckRangeElem);
	
	frag.appendChild(wrap);


	// Items
	wrap = document.createElement('DIV');
	wrap.style.marginLeft = '0.5em';
	wrap.style.display = 'flex';
	wrap.style.flexFlow = 'row wrap';
	wrap.style.flex = 1;
	wrap.style.justifyContent = 'space-between';
	wrap.style.fontSize = '90%';
	
	div = document.createElement('DIV');
	div.innerHTML = 'Items:';
	div.style.flex = '0 1 auto';
	wrap.appendChild(div);
	displayItemsElem.innerHTML = cardFaceKeys[settings.cardFaceIndex];
	displayItemsElem.style.fontWeight = 'bold';
	displayItemsElem.style.textAlign = 'right';
	wrap.appendChild(displayItemsElem);

	itemsRangeElem.style.width = '100%';
	itemsRangeElem.setAttribute('type', 'range');
	itemsRangeElem.setAttribute('min', 0);
	itemsRangeElem.setAttribute('max', cardFaceKeys.length-1 );
	itemsRangeElem.setAttribute('value', settings.cardFaceIndex);
	itemsRangeElem.addEventListener('input', itemsRangeChange);	// change
	wrap.appendChild(itemsRangeElem);

	frag.appendChild(wrap);

	menuElem.appendChild(frag);
	wrap = div = button = input = label = frag = null;
}
this.resize = function () {

	menuElem.style.fontSize = Math.max(Math.floor(fontSize * (boardW/700) ), 8) + 'px';
	setBoardSizes();	// to set new Height value
};

this.getElem = function () {
	return menuElem;
};

function deckRangeChange (e) {

	let range = settings.deckRange;
	if (e && e.target)	range = parseInt(e.target.value, 10) || settings.deckRange;

	setDeckRange(range);

	resetGame();	// from game
}



function itemsRangeChange (e) {
	let val = parseInt(e.target.value, 10) || settings.cardFaceIndex;

	displayItemsElem.innerHTML = cardFaceKeys[val];
	settings.cardFaceIndex = val;
	resetGame();
}

this.setDeckRange = setDeckRange;
function setDeckRange (range) {

	settings.deckRange = range;
	deckRangeElem.value = range;
	settings.deckLength = range2len(range);
	displayDeckLengthElem.innerHTML = settings.deckLength;
}

this.setScore = function (s) {
	scoreElem.innerHTML = s;
};
this.setTotal = function (t) {
	totalElem.innerHTML = t;
};
this.setTime = function (t) {
	timeOutElem.innerHTML = Math.floor(t/1000);
};

this.setStartButton = function (s) {
	startButton.innerHTML = s;
};

init();
}// ------------------------- end Menu {} ---------------------------



init();
} // ------------------ end MemoryGame()-------------------------