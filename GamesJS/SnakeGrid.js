/**
 * 
 * @param {
 
 } outerElem 
 * @param {*} gl 
 */
function SnakeGrid (outerElem, gl) {
'use strict';

	if (!outerElem || typeof outerElem !== 'object') {
		return false;
	}	
	
let gaming = false;
let boardCols = 10;
let boardRows = 9;
let totalSquares = boardCols * boardRows;
let boardHeight = 0;
let boardWidth = 0;
let squareSize = 0;

const grid = [];	// hold snake body
const snake = {
	'chain' : new gl.NodeChain(),		// necu []
	'color' : new Color(255, 255, 255, 100, 10),
	'cloneColor' : function () {return new Color(255, 255, 255, 100, 10);},
	'defaultSpeed' : 800,
	'speed' : 800,
	'maxSpeed' : 100,
	'keyDir' : {'x' : 0, 'y' : 0},	// first direction
	'lastDir' : {'x' : 0, 'y' : 0},
	'eat' : null,
	'food' : null,
};
let score = 0;

// let direction = {x: 0, y: -snakeSpeed};	// udlr
//const keys = [];
const keys = new gl.NodeChain();	// remember keys
let skipTime = 0;
let oldTime = 0;




const container = document.createElement('DIV');
const menu = new Menu(container);
const boardElem = document.createElement('DIV');

function init () {

	container.style.userSelect = 'none';
	container.style.overflow = 'hidden';
	container.style.position = 'relative';
	container.style.minWidth = '320px';
	container.style.minHeight = '320px';
	container.style.width = '100%';
	container.style.height = '100%';
	container.style.display = 'flex';
	container.style.flexFlow = 'column nowrap';

	boardElem.tabIndex = 0;
	boardElem.style.boxSizing = 'border-box';
	boardElem.style.padding = 0;
	boardElem.style.userSelect = 'none';
	boardElem.style.overflow = 'hidden';
	boardElem.style.position = 'relative';
	boardElem.style.flexGrow = 1;
	boardElem.style.backgroundColor = '#dfef10';
	boardElem.style.display = 'flex';
	boardElem.style.flexFlow = 'column nowrap';
	boardElem.style.justifyContent = 'center';
	boardElem.style.alignItems = 'center';
	boardElem.style.alignContent = 'center';
	boardElem.addEventListener('keydown', onKeyDown);
	boardElem.addEventListener('keyup', onKeyUp);
	boardElem.addEventListener('blur', function () {
		pauseGame();
	});

	container.appendChild(boardElem);


	outerElem.innerHTML = '';
	outerElem.appendChild(container);

	getBoardSize();	// must be after container goes to dom

	// must be avter container goes to dom
	createBoard();
	resetGame();

	window.addEventListener('resize', function () {
	
		if (boardHeight != boardElem.clientHeight ||
			boardWidth != boardElem.clientWidth)
		{
			getBoardSize();
			resizeBoard();	
		}
	});

}// ---------------------------- end init () -----------------------------------



function onKeyDown (e) {

	let dir = getUDLR(e.key || e.which || e.keyCode);

	if (keys.indexOf(dir) >= 0)	// || e.which === 32)
	{
		e.preventDefault();	
		return;
	}
	
	keys.push(dir);
	setDirection(dir);

	// block all other keys Esc, F5...
	e.preventDefault();
}

function onKeyUp (e) {

	if (e.key === ' ' || e.which === 32)
	{
		if (gaming)		pauseGame();
		else			playGame();	
	}


	let dir = getUDLR(e.key || e.which || e.keyCode);

//	if (e.which === 32)	return;

	let index = keys.indexOf(dir);
	if (index < 0)
	{
		e.preventDefault();		// block tab, F5, Esc
		return;					
	}
	keys.popIndex(index);

	if (keys.peekTail())	
	{
		setDirection(keys.peekTail());
	}

	e.preventDefault();
}


function setDirection (udlr) {

	let x = snake.lastDir.x,
		y = snake.lastDir.y;

	if (udlr === 'u' && snake.lastDir.y !== -1)
	{
		x = 0;
		y = 1;
	}
	else if (udlr === 'd' && snake.lastDir.y !== 1)
	{
		x = 0;
		y = -1;
	}
	else if (udlr === 'l' && snake.lastDir.x !== 1)
	{
		x = -1;
		y = 0;
	}
	else if (udlr === 'r' && snake.lastDir.x !== -1)
	{
		x = 1;
		y = 0;
	}

	snake.keyDir.x = x;
	snake.keyDir.y = y;
}
// return u d l r 
function getUDLR (code) {

		switch (code)
		{
		case 38:	//	up arrow
		case 'ArrowUp':
		case 87:	//	W
		case 119:	//	w
		case 'w':
			return 'u';

		case 40:	// down arrow
		case 'ArrowDown':
		case 83:	// S
		case 115:	// s
		case 's':
			return 'd';
	
		case 37:	// left arrow
		case 'ArrowLeft':
		case 65:	// A
		case 97:	// a
		case 'a':
			return 'l';
	
		case 39:	// right arrow
		case 'ArrowRight':
		case 68:	// D
		case 100:	// d
		case 'd':
			return 'r';

		default:
		return '';
		}
}

function getBoardSize () {

	boardHeight = boardElem.clientHeight;
	boardWidth = boardElem.clientWidth;
	squareSize = getSquareSize(); 
}
function getSquareSize () {

	let space = 0;
	let h = Math.floor(boardHeight / boardRows) - space;
	let w = Math.floor(boardWidth / boardCols) - space;

	return w < h ? w : h;
}
function resizeBoard () {

	for (let row = 0; row < boardRows; ++row)
	{
		for (let col = 0; col < boardCols; ++col)
		{
			let div = grid[row][col];
			div.style.width = squareSize;
			div.style.height = squareSize;
			div = null;
		}
	}
}
function createBoard () {

	boardElem.innerHTML = '';
	grid.length = 0;

	let frag = document.createDocumentFragment();

	getBoardSize();

	let sizeMin = getSquareSize();


	for (let row = 0; row < boardRows; ++row)
	{
		grid[row] = [];
		let rowElem = document.createElement('DIV');
		rowElem.style.display = 'flex';
		rowElem.style.flexFlow = 'row nowrap';

		for (let col = 0; col < boardCols; ++col)
		{
			let div = document.createElement('DIV');
			div.innerHTML = row * boardCols + col;
			div.style.boxSizing = 'border-box';
			div.style.width = sizeMin + 'px';
			div.style.height = sizeMin + 'px';
			div.style.display = 'flex';
			div.style.justifyContent = 'center';
			div.style.alignItems = 'center';
			div.style.border = '1px solid red';
			div.style.position = 'relative';
			div.style.overflow = 'hidden';
			grid[row][col] = div;
			rowElem.append(div);
			div = null;	
		}

		frag.append(rowElem);
		rowElem = null;
	}

	boardElem.appendChild(frag);

frag = null;
}


function pauseGame () {

	menu.startButtonTxt('Play');
	gaming = false;
	oldTime = 0;
//	cancelAnimationFrame(rafID);
}
function playGame () {

	menu.startButtonTxt('Pause');
	gaming = true;
	oldTime = 0;
//	console.log(performance.now());
	
	boardElem.focus();
	requestAnimationFrame(animate);
}
function resetGame () {

	gaming = false;
	oldTime = 0;
	keys.clear();
	snake.keyDir.x = 0;
	snake.keyDir.y = 0;
	snake.speed = snake.defaultSpeed;
	snake.chain.clear();
	score = 0;

	snake.food = null;
	snake.eat = null;

	menu.setScore(score);

	for (let r = 0; r < boardRows; ++r)
	{
		for (let c = 0; c < boardCols; ++c)
		{
			grid[r][c].innerHTML = '';
		}
	}

	let sp = new SnakePart(Math.floor(boardRows/2), Math.floor(boardCols/2), snake.cloneColor());
	snake.chain.push(sp);
	sp.draw();

	rndFood();

	menu.startButtonTxt('Play');

}




function rndFood () {

	let row = Math.floor(Math.random() * boardRows);
	let col = Math.floor(Math.random() * boardCols);
	let color = snake.color.rndFoodColor();

	snake.food = new SnakePart(row, col, color);
	snake.food.draw();
}

function animate (time) {
	if (!gaming)	return;
	if (oldTime <= 0)	oldTime = time;
	let dt = time - oldTime;							// delta time
	oldTime = time;
//	console.log(dt);

	skipTime += dt;
//	console.log(skipTime);
	if (skipTime < snake.speed) {
		requestAnimationFrame(animate);
		return;
	}
	skipTime = 0;

	snake.lastDir.x = snake.keyDir.x;
	snake.lastDir.y = snake.keyDir.y;

	let head = snake.chain.peekHead();
	let dead = false;
	let sp = null;


	// move body
	let nextRow = head.row;
	let nextCol = head.col;
	snake.chain.setPointer(1);
	sp = snake.chain.next();
	while(sp)
	{
		let oldR = sp.row;
		let oldC = sp.col;

		sp.move(nextRow, nextCol);
		nextRow = oldR;
		nextCol = oldC;
		sp = snake.chain.next();
	}

	// move head
	head.row -= snake.keyDir.y;
	head.col += snake.keyDir.x;

//console.log(moveRow, moveCol);

	if (head.row < 0 || head.row >= boardRows	||				// row bounds	
		head.col < 0 || head.col >= boardCols					// col bounds
	)	dead = true;

	snake.chain.setPointer(1);
	while(!dead && (sp = snake.chain.next()))
	{
		if (head.row === sp.row && head.col === sp.col)			// self bite
		{
			dead = true;
		}
	}

	if (dead) 
	{
		die();
		return;
	}



	if (head.row === snake.food.row && head.col === snake.food.col)		// food
	{
		menu.setScore(++score);
	//	speedUp(10);
		snake.eat = snake.food;
	}




	if (snake.eat &&
		snake.chain.peekTail().row === snake.eat.row &&
		snake.chain.peekTail().col === snake.eat.col)
	{
		menu.setScore(++score);
		speedUp(10);
		console.log(snake.speed);
		snake.chain.peekTail().eat(snake.eat);
		snake.eat = null;
		rndFood();
	}

	head.draw();

	requestAnimationFrame(animate);
}

function speedUp (v) {

	snake.speed -= v;
	snake.speed = snake.speed < snake.maxSpeed ? snake.maxSpeed : snake.speed;
}


function die () {

//	console.log('died:');
gl.msgBox(`Well... Your snake died.
Better luck next time.

`, 'ok', function() {resetGame();});

}

/**
 * 
 * @param {Number} r red
 * @param {Number} g green
 * @param {number} b blue
 * @param {number} rr radius
 * @param {number} z zIndex
 */
function Color (r, g, b, rr = 100, z = 10) {

	this.r = r;
	this.g = g;
	this.b = b;
	this.rr = rr;
	this.z = z;

this.rndFoodColor = function () {
return new Color (Math.floor(Math.random() * 256),
	Math.floor(Math.random() * 256),
	Math.floor(Math.random() * 256),
	50, 20);	// food zIndex
};

this.toRGB = function () {
return 'rgb('+this.r+','+this.g+','+this.b+')';
};

}// ---------------------------- end Color () ----------------------------------



function SnakePart (row, col, colr) {

	this.row = row;
	this.col = col;
	this.color = colr;
	let elem = document.createElement('DIV');
	let that = this;

function init () {

	elem.style.position = 'absolute';
	elem.style.zIndex = that.color.z;
	elem.style.height = that.color.rr + '%';
	elem.style.width = that.color.rr + '%';
	elem.style.backgroundColor = that.color.toRGB();
	elem.style.borderRadius = that.color.rr + '%';
	if (that.color.z === 20)
	{
		elem.style.border = '1px solid black';
	}
}

this.eat = function (part) {

	part.color.z = snake.color.z;
	part.color.rr = snake.color.rr;
	snake.chain.pushTail(part);
};

this.deleteColor = function () {

	grid[this.row][this.col].innerHTML = '';
};

this.draw = draw;
function draw () {
	elem.style.width = that.color.rr +'%';
	elem.style.height = that.color.rr +'%';
	elem.style.zIndex = that.color.z;
	if (that.color.z === 10)	elem.style.border = '';
//	console.log(color.toRGB());

	elem.style.backgroundColor = that.color.toRGB();
	grid[that.row][that.col].appendChild(that.getElem());	// moves elem
}


function colorMove () {

	for (let key of ['r', 'g', 'b'])
	{
		let value = that.color[key] > snake.color[key] ? that.color[key] - 1 : that.color[key] + 1;
		value = value < 0 ? 0 : value;
		value = value > 255 ? 255 : value;
		that.color[key] = value;
	}
}

this.getElem = function () {
	return elem;
};

/**
 * mover snake part
 * @param {Number} row row in grid
 * @param {Number} col column in grid
 */
this.move = function (row, col) {

	this.row = row;
	this.col = col;
	colorMove();
	
	draw();
};

init();
}//------------------------------- SnakePart() ---------------------------------




function Menu (container) {

	let fontSize = 20;
	const menuElem = document.createElement('DIV');

	const fullScreenButton = new gl.FullScreenButton(container);
	const startButtonElem = document.createElement('BUTTON');
	const scoreElem = document.createElement('DIV');
	const boardSizeElem = document.createElement('input');
	const displayBoardSizeElem = document.createElement('DIV');

function init () {

	var wrap, div;

	menuElem.style.userSelect = 'none';
	menuElem.style.overflow = 'hidden';
	menuElem.style.position = 'relative';
	menuElem.style.display = 'flex';
	menuElem.style.justifyContent = 'space-around';
	menuElem.style.padding = '5px';
	menuElem.style.backgroundColor = '#ccc';
	menuElem.style.fontSize = fontSize;
//	menuElem.innerHTML = 'menu';

	wrap = document.createElement('DIV');
	wrap.style.display = 'flex';
	wrap.style.flex = 1;
	wrap.style.justifyContent = 'space-around';

	// FULL SCREEN
	wrap.appendChild(fullScreenButton.getElem());

	// START BUTTON
	startButtonElem.innerHTML = 'Play';
	startButtonElem.style.fontSize = 'inherit';
	startButtonElem.addEventListener('click', function (e) {
	
		if (gaming)		pauseGame();
		else			playGame();
	});
	wrap.appendChild(startButtonElem);







	div = document.createElement('LABEL');
	div.fontSize = 'inherit';
	div.innerHTML = 'Score:';
	div.style.display = 'flex';
	div.style.alignItems = 'center';
	div.style.flexFlow = 'column nowrap';
	scoreElem.innerHTML = score;
	div.append(scoreElem);
//	div.style.border = '1px solid red';
	wrap.append(div);
	menuElem.appendChild(wrap);


	wrap = document.createElement('DIV');
	wrap.style.display = 'flex';
	wrap.style.flex = 1;
	wrap.style.flexFlow = 'column nowrap';
	wrap.style.justifyContent = 'space-around';

	// DISPLAY SIZE
	displayBoardSizeElem.innerHTML = boardRows + 'x' + boardCols + ' = ' + totalSquares;
	wrap.appendChild(displayBoardSizeElem);

	// SIZE
	boardSizeElem.type = 'range';
	boardSizeElem.style.width = '100%';
	boardSizeElem.max = 30;
	boardSizeElem.min = 4;
	boardSizeElem.value = 9;
	boardSizeElem.addEventListener('input', function (e) {
	
		boardRows = parseInt(e.target.value, 10);
		boardCols = boardRows + 1;
		totalSquares = boardCols * boardRows;

		displayBoardSizeElem.innerHTML = boardRows + 'x' + boardCols + ' = ' + totalSquares;

		createBoard();
		resetGame();
	});

	wrap.appendChild(boardSizeElem);

	menuElem.appendChild(wrap);
	container.appendChild(menuElem);

	wrap = div = null;
} // ----------- end init()-------------

this.setScore = function (s) {
	scoreElem.innerHTML = s;
};

this.startButtonTxt = function (str) {

	startButtonElem.innerHTML = str;
};


init();
}// ------------------------------- end Menu()----------------------------------


init();
} // ------------------ end Snake ()-------------------------
